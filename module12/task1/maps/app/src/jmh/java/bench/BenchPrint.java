package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import maps.*;

public class BenchPrint {

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Print {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        static Integer[] hmi;
        static Integer[] lhmi;
        static Integer[] tmi;
        static Integer[] hti;

        @State(Scope.Thread)
        public static class StateHm {
            @Setup(Level.Iteration)
            public void setup() {
                hm = new HashMap<String, Integer>();
                GenerationMap.generate(hm);
                hmi = ParametersGetter.getValues(hm);
                Sort.quickSort(hmi, 0, hmi.length - 1);
            }
        }

        @State(Scope.Thread)
        public static class StateLhm {
            @Setup(Level.Iteration)
            public void setup() {
                lhm = new LinkedHashMap<String, Integer>();
                GenerationMap.generate(lhm);
                lhmi = ParametersGetter.getValues(lhm);
                Sort.quickSort(lhmi, 0, lhmi.length - 1);
            }
        }

        @State(Scope.Thread)
        public static class StateTm {
            @Setup(Level.Iteration)
            public void setup() {
                tm = new TreeMap<String, Integer>();
                GenerationMap.generate(tm);
                tmi = ParametersGetter.getValues(tm);
                Sort.quickSort(tmi, 0, tmi.length - 1);
            }
        }

        @State(Scope.Thread)
        public static class StateHt {
            @Setup(Level.Iteration)
            public void setup() {
                ht = new Hashtable<String, Integer>();
                GenerationMap.generate(ht);
                hti = ParametersGetter.getValues(ht);
                Sort.quickSort(hti, 0, hti.length - 1);
            }
        }

        @Benchmark
        public static void checkQuickSortHashMap(StateHm stateHm) {
            PrintMap.print(hmi);
        }

        @Benchmark
        public static void checkQuickSortLinkedHashMap(StateLhm stateLhm) {
            PrintMap.print(lhmi);
        }

        @Benchmark
        public static void checkQuickSortTreeMap(StateTm stateTm) {
            PrintMap.print(tmi);
        }

        @Benchmark
        public static void checkQuickSortHashtable(StateHt stateHt) {
            PrintMap.print(hti);
        }
    }
}