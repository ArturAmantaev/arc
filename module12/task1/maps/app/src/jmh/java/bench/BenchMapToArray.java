package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import maps.*;

public class BenchMapToArray {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class MapToArray {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
            GenerationMap.generate(hm);
            GenerationMap.generate(lhm);
            GenerationMap.generate(tm);
            GenerationMap.generate(ht);
        }

        @Benchmark
        public static void checkQuickSortHashMap() {
            Integer[] values = ParametersGetter.getValues(hm);
        }

        @Benchmark
        public static void checkQuickSortLinkedHashMap() {
            Integer[] values = ParametersGetter.getValues(lhm);
        }

        @Benchmark
        public static void checkQuickSortTreeMap() {
            Integer[] values = ParametersGetter.getValues(tm);
        }

        @Benchmark
        public static void checkQuickSortHashtable() {
            Integer[] values = ParametersGetter.getValues(ht);
        }
    }
}