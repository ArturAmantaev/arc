package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import maps.*;

public class BenchGeneration {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Generations {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
        }


        @Benchmark
        public static void checkGenerationHashMap() {
            GenerationMap.generate(hm);
        }

        @Benchmark
        public static void checkGenerationLinkedHashMap() {
            GenerationMap.generate(lhm);
        }

        @Benchmark
        public static void checkGenerationTreeMap() {
            GenerationMap.generate(tm);
        }

        @Benchmark
        public static void checkGenerationHashtable() {
            GenerationMap.generate(ht);
        }

    }
}