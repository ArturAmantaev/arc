package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import maps.*;

public class BenchAll {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Maps {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
        }


        @Benchmark
        public static void checkLinkedHashMap() {
            App.checkMap(hm);
        }

        @Benchmark
        public static void checkHashMap() {
            App.checkMap(lhm);
        }

        @Benchmark
        public static void checkTreeMap() {
            App.checkMap(tm);
        }

        @Benchmark
        public static void checkHashtable() {
            App.checkMap(ht);
        }
    }
}
