package maps;

/**
 * класс для сортировки массива
 */
public class Sort {

    /**
     * быстрая сортировка массива
     * begin = 0, end = arr.length - 1
     * @param arr - массив , который необходимо отсортировать
     * @param begin - необходимо передавать 0, чтобы отсортировать массив
     * @param end - необходимо передавать arr.length - 1, чтобы отсортировать массив
     */
    public static void quickSort(Integer[] arr, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            quickSort(arr, begin, partitionIndex-1);
            quickSort(arr, partitionIndex+1, end);
        }
    }

    /**
     * вспомогательный метод для быстрой сортировки
     * @param arr - массив
     * @param begin - старт
     * @param end - конец
     */
    private static int partition(Integer[] arr, int begin, int end) {
        int pivot = arr[end];
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;

                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        int swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;

        return i+1;
    }
}
