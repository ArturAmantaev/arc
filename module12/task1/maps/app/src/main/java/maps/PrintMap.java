package maps;

import java.util.Map;

/**
 * класс для выводя массива в консоль
 */
public class PrintMap {

    /**
     * метод для выода массива в консоль
     * @param integers - массив чисел, которые нужно вывести
     */
    public static void print(Integer[] integers) {
        for (int i = 0; i < integers.length; i++) {
            System.out.println(integers[i]);
        }
    }
}
