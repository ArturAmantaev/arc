package maps;

import java.util.Collection;
import java.util.Map;

/**
 * класс для извлечения массива значений из Map
 */
public class ParametersGetter {

  /**
   * метод для получения массива значений из Map
   */
    public static Integer[] getValues(Map map) {
        Collection<Integer> collection = map.values();
        return collection.toArray(new Integer[0]);
    }
}
