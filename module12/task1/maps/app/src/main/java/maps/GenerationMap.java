package maps;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Random;

/**
 * Класс для генерации Map со значениями
 */
public class GenerationMap {

    /** мксимальная длина строки в Map */
    public static int maxLengthString = 10;

    /** максимальное число в Map */
    public static int maxNumber = 1_000;

    /** размер Map */
    public static int mapSize = 2_000_000;

    /**
     * метод заполняющий Map
     * @param map - Map, который необходимо заполнить
     * @return Map - тот же Map
     */
    public static Map generate(Map map) {
        for (int i = 0; i < mapSize; i++) {
            map.put(generateString(), generateNumber());
        }
        return map;
    }

    /**
     * метод генерирующий строку
     * @return String - сгенерированная строка
     */
    private static String generateString() {
        byte[] array = new byte[maxLengthString];
        new Random().nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }

    /**
     * метод генерирующий число
     * @return Integer - сгенерированное число
     */
    private static Integer generateNumber() {
        return (Integer) (int) (Math.random() * maxNumber);
    }
}
