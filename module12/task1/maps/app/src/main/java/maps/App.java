package maps;

import java.util.*;

/**
 * класс с main методом
 */
public class App {
    /** Количество тестов, которые будут запущены */
    public static int numberTests = 5;

    /** Количество Map-ов */
    public static int numberMaps = 4;

    /**
     * main метод, выводящый в консоль результаты выполнения по времени
     * @param args
     */
    public static void main(String[] args) {
        Map<String, ArrayList<Long>> mapGeneration = new HashMap<>();
        Map<String, ArrayList<Long>> mapToArray = new HashMap<>();
        Map<String, ArrayList<Long>> mapSort = new HashMap<>();
        Map<String, ArrayList<Long>> mapPrint = new HashMap<>();
        Map<String, ArrayList<Long>> mapResult = new HashMap<>();

        Map[] mapsResult = {mapGeneration, mapToArray, mapSort, mapPrint, mapResult};

        for (int i = 0; i < mapsResult.length; i++) {
            for (int j = 0; j < numberMaps; j++) {
                mapsResult[j].put(findClass(i).getClass().getSimpleName(), new ArrayList<Double>());
            }
        }

        for (int i = 0; i < numberTests; i++) {
            long timeStart;
            long timeStartAll;

            for (int j = 0; j < numberMaps; j++) {
                Map<Integer, String> map = findClass(i);
                String mapName = map.getClass().getSimpleName();

                timeStartAll = timeStart = System.currentTimeMillis();
                GenerationMap.generate(map);
                mapGeneration.get(mapName).add(System.currentTimeMillis() - timeStart);

                timeStart = System.currentTimeMillis();
                Integer[] integers = ParametersGetter.getValues(map);
                mapToArray.get(mapName).add(System.currentTimeMillis() - timeStart);

                timeStart = System.currentTimeMillis();
                Sort.quickSort(integers, 0, integers.length - 1);
                mapSort.get(mapName).add(System.currentTimeMillis() - timeStart);

                timeStart = System.currentTimeMillis();
                PrintMap.print(integers);
                mapPrint.get(mapName).add(System.currentTimeMillis() - timeStart);

                mapResult.get(mapName).add(System.currentTimeMillis() - timeStartAll);
            }
        }
    }

    /**
     * метод для получения Map по индексу
     * @param i
     * @return
     */
    public static Map findClass(int i) {
        Map map;
        switch (i) {
            case 0: {
                map = new HashMap<>();
                break;
            }
            case 1: {
                map = new TreeMap<>();
                break;
            }
            case 2: {
                map = new LinkedHashMap<>();
                break;
            }
            default: {
                map = new Hashtable<>();
                break;
            }
        }
        return map;
    }

    /**
     * полная проверка Map
     * @param map - Map, который нужно проверить
     */
    public static void checkMap(Map map) {
        GenerationMap.generate(map);
        Integer[] integers = ParametersGetter.getValues(map);
        Sort.quickSort(integers, 0, integers.length - 1);
        PrintMap.print(integers);
    }
}
