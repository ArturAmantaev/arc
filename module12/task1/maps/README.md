| Benchmark                                      | Mode | Cnt | Score   Error  | Units |
|------------------------------------------------|------|-----|----------------|-------|
| Bench.Generations.checkGenerationHashMap       | avgt | 5   | 1,304 ± 0,085  | s/op  |
| Bench.Generations.checkGenerationHashtable     | avgt | 5   | 1,205 ± 0,259  | s/op  |
| Bench.Generations.checkGenerationLinkedHashMap | avgt | 5   | 1,640 ± 0,068  | s/op  |
| Bench.Generations.checkGenerationTreeMap       | avgt | 5   | 3,517 ± 0,707  | s/op  |
| Bench.MapToArray.checkQuickSortHashMap         | avgt | 5   | 0,076 ± 0,109  | s/op  |
| Bench.MapToArray.checkQuickSortHashtable       | avgt | 5   | 0,101 ± 0,106  | s/op  |
| Bench.MapToArray.checkQuickSortLinkedHashMap   | avgt | 5   | 0,031 ± 0,051  | s/op  |
| Bench.MapToArray.checkQuickSortTreeMap         | avgt | 5   | 0,078 ± 0,090  | s/op  |
| Bench.Maps.checkHashMap                        | avgt | 5   | 31,623 ± 0,860 | s/op  |
| Bench.Maps.checkHashtable                      | avgt | 5   | 31,256 ± 0,516 | s/op  |
| Bench.Maps.checkLinkedHashMap                  | avgt | 5   | 31,079 ± 0,582 | s/op  |
| Bench.Maps.checkTreeMap                        | avgt | 5   | 33,619 ± 1,109 | s/op  |
| Bench.Print.checkQuickSortHashMap              | avgt | 5   | 6,306 ± 0,473  | s/op  |
| Bench.Print.checkQuickSortHashtable            | avgt | 5   | 6,237 ± 0,333  | s/op  |
| Bench.Print.checkQuickSortLinkedHashMap        | avgt | 5   | 6,328 ± 0,306  | s/op  |
| Bench.Print.checkQuickSortTreeMap              | avgt | 5   | 6,316 ± 0,130  | s/op  |
| Bench.Sorts.checkQuickSortHashMap              | avgt | 5   | 23,989 ± 0,297 | s/op  |
| Bench.Sorts.checkQuickSortHashtable            | avgt | 5   | 24,369 ± 0,175 | s/op  |
| Bench.Sorts.checkQuickSortLinkedHashMap        | avgt | 5   | 24,018 ± 0,118 | s/op  |
| Bench.Sorts.checkQuickSortTreeMap              | avgt | 5   | 24,448 ± 0,327 | s/op  |
