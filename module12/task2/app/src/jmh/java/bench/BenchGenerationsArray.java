package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import task2.*;

public class BenchGenerationsMaps {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class GenerationsArray {

        @Benchmark
        public static void checkQuickSortHashMap() {
            Generator.generateArray();
        }
    }
}