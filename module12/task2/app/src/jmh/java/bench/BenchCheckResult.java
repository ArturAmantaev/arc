package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import task2.*;

public class BenchGenerationsMaps {

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class CheckResult {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        static String[] array;

        @State(Scope.Thread)
        public static class StateHm {
            @Setup(Level.Iteration)
            public void setup() {
                hm = new HashMap<String, Integer>();
                Generator.generateMap(hm);
            }
        }

        @State(Scope.Thread)
        public static class StateLhm {
            @Setup(Level.Iteration)
            public void setup() {
                lhm = new LinkedHashMap<String, Integer>();
                Generator.generateMap(lhm);
            }
        }

        @State(Scope.Thread)
        public static class StateTm {
            @Setup(Level.Iteration)
            public void setup() {
                tm = new TreeMap<String, Integer>();
                Generator.generateMap(tm);
            }
        }

        @State(Scope.Thread)
        public static class StateHt {
            @Setup(Level.Iteration)
            public void setup() {
                ht = new Hashtable<String, Integer>();
                Generator.generateMap(ht);
            }
        }

        @Setup(Level.Iteration)
        public void setup() {
            array = Generator.generateArray();
        }

        @Benchmark
        public static void checkQuickSortHashMap(StateHm stateHm) {
            App.checkResult(hm, array);
        }

        @Benchmark
        public static void checkQuickSortLinkedHashMap(StateLhm stateLhm) {
            App.checkResult(lhm, array);
        }

        @Benchmark
        public static void checkQuickSortTreeMap(StateTm stateTm) {
            App.checkResult(tm, array);
        }

        @Benchmark
        public static void checkQuickSortHashtable(StateHt stateHt) {
            App.checkResult(ht, array);
        }
    }
}