package task2;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.Random;

/**
 * Класс для генерации Map со значениями
 */
public class Generator {

    /** максимальная длина строки в Map */
    public static int maxLengthString = 10;

    /** размер Map */
    public static int mapSize = 2_000_000;

    /** размер массива строк */
    public static int arraySize = 30_000;

    /**
     * метод, заполняющий Map
     * @param map - Map, который необходимо заполнить
     * @return Map - тот же Map
     */
    public static Map generateMap(Map map) {
        for (int i = 0; i < mapSize; i++) {
            map.put(generateString(maxLengthString), generateNumber(Integer.MAX_VALUE));
        }
        return map;
    }

    /**
     * метод, генерирующий строку
     * @param length - длина сгенерированной строки
     * @return String - сгенерированная строка
     */
    public static String generateString(int length) {
        byte[] array = new byte[length];
        new Random().nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }

    /**
     * метод, генерирующий число
     * @param maxNumber - максимально возможное сгенерированное число
     * @return Integer - сгенерированное число
     */
    public static Integer generateNumber(int maxNumber) {
        return (Integer) (int) (Math.random() * maxNumber);
    }

    /**
     * метод, генерирующий массив строк
     * @return String[] - сгенерированный массив строк
     */
    public static String[] generateArray() {
        String[] array = new String[arraySize];
        for (int i = 0; i < array.length; i++) {
            array[i] = generateString(maxLengthString);
        }
        return array;
    }
}
