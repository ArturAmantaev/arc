package task2;

import java.util.*;

/**
 * класс с main методом
 */
public class App {

    /** количество Map-ов */
    private static int numberMaps = 4;

    /** количество тестов */
    public static int numberTests = 1;

    /**
     * main метод, выводящий в консоль результаты выполнения по времени
     */
    public static void main(String[] args) {
        Map<String, ArrayList<Long>> generateMapTime = new HashMap<>();
        Map<String, ArrayList<Long>> generateArrayTime = new HashMap<>();
        Map<String, ArrayList<Long>> checkResultTime = new HashMap<>();
        Map<String, ArrayList<Long>> resultTime = new HashMap<>();
        Map[] mapsResult = {generateMapTime, generateArrayTime, checkResultTime, resultTime};
        for (int i = 0; i < mapsResult.length; i++) {
            for (int j = 0; j < numberMaps; j++) {
                mapsResult[i].put(findClass(j).getClass().getSimpleName(), new ArrayList<Long>());
            }
        }

        for (int i = 0; i < numberTests; i++) {
            long timeStart;
            long timeStartAll;
            for (int j = 0; j < numberMaps; j++) {
                Map<String, Integer> map = findClass(j);
                String mapName = map.getClass().getSimpleName();

                timeStartAll = timeStart = System.currentTimeMillis();
                Generator.generateMap(map);
                generateMapTime.get(mapName).add(System.currentTimeMillis() - timeStart);

                timeStart = System.currentTimeMillis();
                String[] array = Generator.generateArray();
                generateArrayTime.get(mapName).add(System.currentTimeMillis() - timeStart);

                timeStart = System.currentTimeMillis();
                checkResult(map, array);
                checkResultTime.get(mapName).add((System.currentTimeMillis() - timeStart));

                resultTime.get(mapName).add(System.currentTimeMillis() - timeStartAll);
            }
        }

        for (int i = 0; i < numberMaps; i++) {
            String mapName = findClass(i).getClass().getSimpleName();
            System.out.println(mapName);
            for (int j = 0; j < mapsResult.length; j++) {
                for (int k = 0; k < numberTests; k++) {
                    System.out.print(mapsResult[j].get(mapName));
                }
                System.out.println();
            }
        }
    }

    /**
     * получение итогового числа из Map и array на основе задания
     * @param map - Map, хранящий ключи: строки; значения: числа
     * @param array - массив с числами
     */
    public static long checkResult(Map<String, Integer> map, String[] array) {
        long result = 0;
        for (int j = 0; j < array.length; j++) {
            if (map.containsKey(array[j])) {
                result += map.get(array[j]);
            }
        }
        return result;
    }

    /**
     * полная проверка Map
     * @param map - Map, который нужно проверить
     */
    public static long checkAll(Map map) {
        Generator.generateMap(map);
        String[] array = Generator.generateArray();
        return checkResult(map, array);
    }

    /**
     * метод для получения Map по индексу
     * @param i
     * @return
     */
    public static Map findClass(int i) {
        Map map;
        switch (i) {
            case 0: {
                map = new HashMap<>();
                break;
            }
            case 1: {
                map = new TreeMap<>();
                break;
            }
            case 2: {
                map = new LinkedHashMap<>();
                break;
            }
            default: {
                map = new Hashtable<>();
                break;
            }
        }
        return map;
    }
}
