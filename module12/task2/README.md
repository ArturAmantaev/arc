| Benchmark                                             | Mode | Cnt | Score    Error | Units |
|-------------------------------------------------------|------|-----|----------------|-------|
| Bench.CheckAll.checkHashMap                           | avgt | 5   | 1,705 ±  0,204 | s/op  |
| Bench.CheckAll.checkHashtable                         | avgt | 5   | 1,175 ±  0,072 | s/op  |
| Bench.CheckAll.checkLinkedHashMap                     | avgt | 5   | 1,341 ±  0,091 | s/op  |
| Bench.CheckAll.checkTreeMap                           | avgt | 5   | 3,556 ±  0,334 | s/op  |
| Bench.CheckResult.checkQuickSortHashMap               | avgt | 5   | 0,002 ±  0,001 | s/op  |
| Bench.CheckResult.checkQuickSortHashtable             | avgt | 5   | 0,002 ±  0,001 | s/op  |
| Bench.CheckResult.checkQuickSortLinkedHashMap         | avgt | 5   | 0,001 ±  0,001 | s/op  |
| Bench.CheckResult.checkQuickSortTreeMap               | avgt | 5   | 0,032 ±  0,006 | s/op  |
| Bench.GenerationsArray.checkQuickSortHashMap          | avgt | 5   | 0,005 ±  0,001 | s/op  |
| Bench.GenerationsMaps.checkGenerationMapHashMap       | avgt | 5   | 1,368 ±  0,045 | s/op  |
| Bench.GenerationsMaps.checkGenerationMapHashtable     | avgt | 5   | 1,178 ±  0,091 | s/op  |
| Bench.GenerationsMaps.checkGenerationMapLinkedHashMap | avgt | 5   | 1,650 ±  0,044 | s/op  |
| Bench.GenerationsMaps.checkGenerationMapTreeMap       | avgt | 5   | 3,500 ±  0,087 | s/op  |
