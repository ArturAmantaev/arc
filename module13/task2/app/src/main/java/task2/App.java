package task2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * класс для нахожддения слов, которые являются "пятыми"
 */
public class App {

    /**
     * регулярное выражение для нахождения таких слов
     */
    public static String regularExpression = "[пП]ят(ы[йемх]|ого|ом[у]?|ыми|ая|ую|ою|ой)\\s+([\\w[\\-]]+)";

    /**
     * метод для нахождения стол, которые являются "пятыми"
     * @param string - строка, по которой ведется поиск
     * @return массив строк - слова, которые являются "пятыми"
     */
    public static String[] getWord(String string) {
        Pattern pattern = Pattern.compile(regularExpression, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(string);
        String[] result = new String[matcher.groupCount()+1];

        for (int i = 0; matcher.find(); i++) {
            result[i] = matcher.group(2);
        }

        return result;
    }
}
