package task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для валидации номера телефона
 */
public class App {

    /**
     * статичное поле, хранящее в себе регулярное выражение
     */
    public static String regularExpression = "\\A(\\+?7|8)[\\(-]?\\d{3}[\\)-]?\\d{3}-?\\d{2}-?\\d{2}\\z";

    /**
     * main метод
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(match("7-800-555-35-35"));
    }

    /**
     * метод, проверяющий строку на валидность регулярному выражению, то есть является ли строка номером телефона
     * @param string - строка, которую необходимо проверить
     * @return true или false - является ли номером телефона или нет
     */
    public static boolean match(String string) {
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
