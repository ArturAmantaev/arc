package task3;

/**
 * класс курильщика
 */
public class Smoker {
    /** минимальное время в мс на курение */
    public static final long MIN_SMOKING_TIME = 5000;

    /** максимальное время в мс на курение */
    public static final long MAX_SMOKING_TIME = 5000;

    /**
     * имя курильщика
     */
    private String name;

    /** ресурс, которым владеет купильщик */
    private Resource resource;

    /** статус курильщика - курит или нет */
    private boolean isSmoke = false;

    /**
     * getter для имени курильщика
     * @return String - имя курильщика
     */
    public synchronized String getName() {
        return name;
    }

    /**
     * setter длля имени курильщика
     * @param name - имя курильщика
     */
    public synchronized void setName(String name) {
        this.name = name;
    }

    /**
     * getter для ресурса
     * @return Resource - ресурс
     */
    public synchronized Resource getResource() {
        return resource;
    }

    /**
     * setter для ресурса
     * @param resource - ресурс
     */
    public synchronized void setResource(Resource resource) {
        this.resource = resource;
    }

    /**
     * проверка курит ли сейчас курильщик
     * @return boolean - курит или нет
     */
    public synchronized boolean isSmoke() {
        return isSmoke;
    }

    /**
     * setter для статуса: курит или нет сейчас курильщик
     * @param smoke - boolean курит или нет
     */
    public synchronized void setSmoke(boolean smoke) {
        isSmoke = smoke;
    }

    /**
     * конструктор курильщика
     * @param name - имя курильщика
     * @param resource - ресурс, которым владеет курильщик
     */
    public Smoker(String name, Resource resource) {
        this.name = name;
        this.resource = resource;
    }

    /**
     * метод добавления ресурса на стол
     * @param table - объект стола
     */
    public void addResourceToTable(Table table) {
        table.addResource(resource);
    }

    /**
     * метод курения
     * @throws InterruptedException
     */
    public void smoke() throws InterruptedException {
        setSmoke(true);
        Thread.sleep(MIN_SMOKING_TIME + (int) (Math.random() * (MAX_SMOKING_TIME - MIN_SMOKING_TIME)));
        setSmoke(false);
    }

    /**
     * метод проверки стола на наличие недостающих ресурсов для сигареты
     * @param table - объект стола
     * @return boolean - нужен владеемый ресурс, для изготовления сигратеры, при использовании ресурсов со стола
     */
    public boolean checkTable(Table table) {
        return table.checkMissingResource(resource);
    }

    /**
     * метод создания сигареты
     * @param table - объект стола
     * @throws Exception
     */
    public void makeCigarette(Table table) throws Exception {
        table.takeResourcesBesides();
    }
}
