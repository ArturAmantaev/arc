package task3;

/**
 * класс потока курильщика
 */
public class SmokerThread implements Runnable {
    /** минимальное время в мс, раз в которое курильщик проверяет стол */
    public static final long MIN_TIME_BETWEEN_CHECK = 1000;

    /** максимальное время в мс, раз в которое курильщик проверяет стол */
    public static final long MAX_TIME_BETWEEN_CHECK = 1000;

    /** объект курильщика */
    public final Smoker smoker;

    /** объект стола */
    public Table table;

    /**
     * конструктор
     * @param smoker
     * @param table
     */
    public SmokerThread(Smoker smoker, Table table) {
        this.smoker = smoker;
        this.table = table;
    }

    /**
     * run метод от интерфейса Runnable
     */
    public void run() {
        try {
            while (true) {
                synchronized (smoker) {
                    if (smoker.checkTable(table)) {
                        System.out.println(smoker.getName() + " берет необходимое и делает сигарету");

                        smoker.makeCigarette(table);
                        System.out.println(smoker.getName() + " идет курить");
                        smoker.smoke();
                        System.out.println(smoker.getName() + " покурил");
                    }
                }
                Thread.sleep(MIN_TIME_BETWEEN_CHECK + (int) (Math.random() * (MAX_TIME_BETWEEN_CHECK - MIN_TIME_BETWEEN_CHECK)));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
