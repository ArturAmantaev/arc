package task3;

/**
 * класс потока бармена
 */
public class BarmanThread implements Runnable {
    /** минимальное время в мс между добавлениями ресурса на стол */
    public static final long MIN_TIME_BETWEEN_ADD_RESOURCE = 1000;

    /** максимальное время в мс между добавлениями ресурса на стол */
    public static final long MAX_TIME_BETWEEN_ADD_RESOURCE = 2000;

    /** объект стола */
    public Table table;

    /** массив курильщиков */
    public Smoker[] smokers;

    /**
     * конструктор
     * @param table - объект стола
     * @param smokers - массив курильщиков
     */
    public BarmanThread(Table table, Smoker[] smokers) {
        this.table = table;
        this.smokers = smokers;
    }

    /**
     * run метод от интерфейса Runnable
     */
    public void run() {
        try {
            while (true) {
                int[] indexes = getArrayBesidesRandomNumber(2);

                for (int i = 0; i < indexes.length; i++) {
                    if (smokers[indexes[i]].isSmoke()) continue;
                    if (table.hasResource(smokers[indexes[i]].getResource())) continue;

                    smokers[indexes[i]].addResourceToTable(table);
                    System.out.println("бармен добавил " + smokers[indexes[i]].getResource());
                }

                Thread.sleep(MIN_TIME_BETWEEN_ADD_RESOURCE + (int) (Math.random() * (MAX_TIME_BETWEEN_ADD_RESOURCE - MIN_TIME_BETWEEN_ADD_RESOURCE)));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * метод получения массива чисел за исключением одного рандомного числа
     * @param max - максимальное число которое может содержаться в массиве
     * @return int[] - массив, длины (max - 1) , содержащий числа от 0 до его длины, за исключением одного рандомного числа
     */
    public static int[] getArrayBesidesRandomNumber(int max) {
        int number = (int) (Math.random() * (max + 1));
        int[] result = new int[max];

        for (int i = 0; i < result.length; i++) {
            result[i] = (number + i + 1) % (max + 1);
        }

        return result;
    }
}
