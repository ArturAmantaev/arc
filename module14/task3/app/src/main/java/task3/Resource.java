package task3;

/**
 * enum ресурсов, необходимых для изготовления сигареты
 */
public enum Resource {
    TOBACCO, PAPER, MATCHES
}
