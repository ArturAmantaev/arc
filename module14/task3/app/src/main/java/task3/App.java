package task3;

/**
 * класс, содержащий main метод
 */
public class App {
    /**
     * main метод, создающий и запускающий потоки
     * @param args
     */
    public static void main(String[] args) {
        Table table = new Table();
        Smoker smokerWithTobacco = new Smoker("sm1", Resource.TOBACCO);
        Smoker smokerWithPaper = new Smoker("sm2", Resource.PAPER);
        Smoker smokerWithMatches = new Smoker("sm3", Resource.MATCHES);

        Thread sm1Thread = new Thread(new SmokerThread(smokerWithTobacco, table));
        Thread sm2Thread = new Thread(new SmokerThread(smokerWithPaper, table));
        Thread sm3Thread = new Thread(new SmokerThread(smokerWithMatches, table));
        Thread barman = new Thread(new BarmanThread(table, new Smoker[]{smokerWithTobacco, smokerWithPaper, smokerWithMatches}));

        sm1Thread.start();
        sm2Thread.start();
        sm3Thread.start();
        barman.start();
    }
}
