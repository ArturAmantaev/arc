package task3;

import java.util.HashMap;
import java.util.Map;

/**
 * класс стола
 */
public class Table {
    public HashMap<Resource, Boolean> resourceBooleanHashMap = new HashMap<>();

    /**
     * конструктор стола
     */
    public Table() {
        Resource[] resources = Resource.values();
        for (int i = 0; i < resources.length; i++) {
            resourceBooleanHashMap.put(resources[i], false);
        }
    }

    /**
     * метод добавления ресурса на стол
     * @param resource - Resource, который нужно добавить на стол
     */
    public synchronized void addResource(Resource resource) {
        resourceBooleanHashMap.put(resource, true);
    }

    /**
     * метод получения недостающих ресурсов со стола
     * @throws Exception
     */
    public synchronized void takeResourcesBesides() throws Exception {
        resourceBooleanHashMap.replaceAll((key, oldValue) -> false);
    }

    /**
     * метод проверки наличия недостающих ресурсов на столе
     * @param resource - Resource, который не нужно учитывать при проверке
     * @return boolean - есть или нет оставшиеся два ресурса
     */
    public synchronized boolean checkMissingResource(Resource resource) {
        for (Map.Entry<Resource, Boolean> item : resourceBooleanHashMap.entrySet()) {
            if (item.getKey() != resource && !item.getValue()) return false;
        }
        return true;
    }

    /**
     * проверка наличия ресурса на столе
     * @param resource - проверяемый ресурс
     * @return
     */
    public synchronized boolean hasResource(Resource resource) {
        return resourceBooleanHashMap.get(resource);
    }
}
