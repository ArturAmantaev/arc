package task1;

/**
 * класс читателя
 */
public class MyReader implements Runnable {
    /** объект данных */
    public MyData myData;

    /** минимальное время в мс между чтениями */
    public static final int MIN_TIME_BETWEEN_READING = 100;

    /** максимальное время в мс между чтениями */
    private static final int MAX_TIME_BETWEEN_READING = 500;

    /** массив символов - прочитанные данные */
    public char[] chars;

    /** статус читатела */
    public ReaderStatus status;

    /**
     * конструктор
     * @param myData - объект MyData (данные)
     */
    public MyReader(MyData myData) {
        this.myData = myData;
    }

    /**
     * run метод от Runnable интерфейса
     */
    public void run() {
        while (true) {
            try {
                status = ReaderStatus.READ;
                chars = myData.read();
                status = ReaderStatus.WAIT;
                System.out.println(Thread.currentThread().getName() + " закончил читать. То что он запомнил: " + String.valueOf(chars));
                Thread.sleep(MIN_TIME_BETWEEN_READING + (int) (Math.random() * (MAX_TIME_BETWEEN_READING - MIN_TIME_BETWEEN_READING)));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
