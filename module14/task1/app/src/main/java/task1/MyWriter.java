package task1;

/**
 * класс писателя
 */
public class MyWriter implements Runnable {
    /** объект данных */
    public MyData myData;

    /** минимальное время в мс между записями */
    private static final int MIN_TIME_BETWEEN_WRITING = 5000;

    /** максимальное время в мс между записями*/
    private static final int MAX_TIME_BETWEEN_WRITING = 10000;

    /** статус писателя */
    public WriteStatus status;

    /**
     * конструктор
     * @param myData - объект MyData (данные)
     */
    public MyWriter(MyData myData) {
        this.myData = myData;
    }

    /**
     * run метод от Runnable интерфейса
     */
    public void run() {
        while (true) {
            try {
                status = WriteStatus.WRITE;
                this.myData.write();
                status = WriteStatus.WAIT;
                Thread.sleep(MIN_TIME_BETWEEN_WRITING + (int) (Math.random() * (MAX_TIME_BETWEEN_WRITING - MIN_TIME_BETWEEN_WRITING)));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
