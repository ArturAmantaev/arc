package task1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

/**
 * класс с main методом
 */
public class App {
    /** количество читателей */
    public static final int readersCount = 4;

    /** количество писателей */
    public static final int writersCount = 2;

    /**
     * main метод
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        MyData myData = new MyData(new char[]{'H', 'e', 'l', 'l', 'o'});
        MyReader[] myReaders = new MyReader[readersCount];
        Thread[] threadsReaders = new Thread[readersCount];

        for (int i = 0; i < threadsReaders.length; i++) {
            myReaders[i] = new MyReader(myData);
            threadsReaders[i] = new Thread(myReaders[i], "r" + i);
        }

        MyWriter[] myWriters = new MyWriter[writersCount];
        Thread[] threadsWriters = new Thread[writersCount];
        for (int i = 0; i < myWriters.length; i++) {
            myWriters[i] = new MyWriter(myData);
            threadsWriters[i] = new Thread(myWriters[i], "w" + i);
        }

        for (int i = 0; i < threadsReaders.length; i++) {
            threadsReaders[i].start();
        }
        for (int i = 0; i < threadsWriters.length; i++) {
            threadsWriters[i].start();
        }
    }
}
