package task1;

import java.util.Random;

/**
 * класс для хранения и получения данных
 */
public class MyData {
    /** строка (некоторые данные) */
    private final char[] chars;

    /** минимальное время чтения одного символа в мс */
    public static final int MIN_READING_TIME = 500;

    /** максимальное время чтения одного символа данных в мс */
    private static final int MAX_READING_TIME = 1000;

    /** минимальное время записи в мс*/
    private static final int MIN_WRITING_TIME = 2000;

    /** максимальное время записи данных в мс */
    private static final int MAX_WRITING_TIME = 4000;

    private static final int MIN_NUMBER_CHANGES = 1;

    /** Булевое значение, идет ли запись сейчас */
    private boolean nowWrite = false;
    /**
     * конструктор
     * @param chars - изначальный массив символов
     */
    public MyData(char[] chars) {
        this.chars = chars;
    }

    /**
     * метод чтения данных
     * @return char[] - прочитанный массив символов
     * @throws InterruptedException
     */
    public char[] read() throws InterruptedException {
        while (nowWrite) {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {}
            }
        }
        char[] result = new char[this.chars.length];
        for (int i = 0; i < this.chars.length; i++) {
            result[i] = this.chars[i];
            System.out.println(Thread.currentThread().getName() + " читает: " + result[i]);
            Thread.sleep(MIN_READING_TIME + (int) (Math.random() * (MAX_READING_TIME - MIN_READING_TIME)));
        }
        return result;
    }

    /**
     * метод записи данных
     * @throws InterruptedException
     */
    public synchronized void write() throws InterruptedException {
        nowWrite = true;
        System.out.println(Thread.currentThread().getName() + " начал писать");
        int numberChanges = MIN_NUMBER_CHANGES + (int) (Math.random() * (this.chars.length - MIN_NUMBER_CHANGES));
        for (int i = 0; i < numberChanges; i++) {
            int j = (int) (Math.random() * this.chars.length);
            this.chars[j] = generateChar();
            System.out.println(Thread.currentThread().getName() + " написал: " + this.chars[j]);
            Thread.sleep(MIN_WRITING_TIME + (int) (Math.random() * (MAX_WRITING_TIME - MIN_WRITING_TIME)));
        }
        System.out.println(Thread.currentThread().getName() + " написал");
        nowWrite = false;
        this.notifyAll();
    }

    /**
     * метод генерации строки
     * @return String - сгенерированная строка
     */
    public static char generateChar() {
        Random rnd = new Random();
        return (char) ('a' + rnd.nextInt(26));
    }
}
