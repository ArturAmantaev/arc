package task4;

/**
 * класс вилки
 */
public class Fork {
    /** лежит ли вилка на столе */
    private boolean lie = true;

    /**
     * лежит ли вилка или нет
     * @return boolean - лежит или нет
     */
    public synchronized boolean isLie() {
        return lie;
    }

    /**
     * метод взятия вилки со стола
     */
    public synchronized void take() {
        this.lie = false;
    }

    /**
     * метод, чтобы положить вилку на стол
     */
    public synchronized void putDown() {
        this.lie = true;
    }
}
