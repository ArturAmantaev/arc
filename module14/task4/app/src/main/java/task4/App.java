package task4;

import java.util.concurrent.Semaphore;

/**
 * класс с main методом
 */
public class App {
    /**
     * main метод
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        int philosopherNumbers = 5;
        Semaphore someoneTakesTheForks = new Semaphore(1);

        Fork[] forks = new Fork[philosopherNumbers];
        for (int i = 0; i < forks.length; i++) {
            forks[i] = new Fork();
        }

        Philosopher[] philosophers = new Philosopher[philosopherNumbers];
        for (int i = 0; i < philosophers.length; i++) {
            philosophers[i] = new Philosopher("Философ-" + i);
        }

        PhilosopherThread[] philosophersThreads = new PhilosopherThread[philosophers.length];
        for (int i = 0; i < philosophersThreads.length; i++) {
            philosophersThreads[i] = new PhilosopherThread(forks[i],
                    forks[(i + philosophersThreads.length - 1) % philosophersThreads.length],
                    philosophers[i],
                    philosophers[(i + 1) % philosophersThreads.length],
                    philosophers[(i + philosophersThreads.length - 1) % philosophersThreads.length],
                    someoneTakesTheForks);
        }

        Thread[] hungers = new Thread[philosophers.length];
        for (int i = 0; i < hungers.length; i++) {
            hungers[i] = new Thread(philosophersThreads[i].new HungerThread());
            new Thread(philosophersThreads[i]).start();
            hungers[i].start();
        }
    }
}
