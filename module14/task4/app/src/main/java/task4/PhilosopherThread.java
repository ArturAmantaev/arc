package task4;

import java.util.concurrent.Semaphore;

/**
 * класс потока философа
 */
public class PhilosopherThread implements Runnable {
    /**
     * минимальное время, чтобы поесть
     */
    public static final int MIN_TIME_TO_EAT = 5000;

    /**
     * максимальное время, чтобы поесть
     */
    public static final int MAX_TIME_TO_EAT = 10000;

    /**
     * минимальное время, чтобы положить вилку
     */
    public static final int MIN_TIME_TO_PUT_DOWN_FORK = 500;

    /**
     * максимальное время, чтобы положить вилку
     */
    public static final int MAX_TIME_TO_PUT_DOWN_FORK = 1000;

    /**
     * объект левой вилки
     */
    public Fork leftFork;

    /**
     * объект правой вилки
     */
    public Fork rightFork;

    /**
     * объект философа
     */
    public Philosopher philosopher;

    /**
     * объект левого философа
     */
    public Philosopher leftPhilosopher;

    /**
     * объект парвого философа
     */
    public Philosopher rightPhilosopher;

    /**
     * объект семафора, обозначающий, что кто-то сейчас берет вилки
     */
    public Semaphore someoneTakesTheForks;

    /**
     * конструктор
     * @param leftFork             - левая вилка
     * @param rightFork            - правая вилка
     * @param philosopher          - философ
     * @param leftPhilosopher      - философ слева
     * @param rightPhilosopher     - философ права
     * @param someoneTakesTheForks - семафор на взятие вилок
     */
    public PhilosopherThread(Fork leftFork, Fork rightFork, Philosopher philosopher, Philosopher leftPhilosopher, Philosopher rightPhilosopher, Semaphore someoneTakesTheForks) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.philosopher = philosopher;
        this.leftPhilosopher = leftPhilosopher;
        this.rightPhilosopher = rightPhilosopher;
        this.someoneTakesTheForks = someoneTakesTheForks;
    }

    /**
     * run метод от Runnable интерфейса
     */
    public void run() {
        try {
            while (true) {
                if (!philosopher.isHunger() && (leftPhilosopher.isHunger() || rightPhilosopher.isHunger())) {
                    System.out.println(philosopher.name + " видит, что сосед голодает, поэтому он пока поразмышляте");
                } else {
                    eat();
                }
                System.out.println(philosopher.name + " начинает размышлять");
                philosopher.meditate();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void eat() throws InterruptedException {
        someoneTakesTheForks.acquire();

        if (!leftFork.isLie() || !rightFork.isLie()) {
            someoneTakesTheForks.release();
            return;
        }
        philosopher.hasMeal = true;
        System.out.println(philosopher.getName() + " хочет взять левую вилку");
        leftFork.take();
        System.out.println(philosopher.getName() + " взял левую вилку");
        System.out.println(philosopher.getName() + " хочет взять правую вилку");
        rightFork.take();
        System.out.println(philosopher.getName() + " взял правую вилку");

        someoneTakesTheForks.release();

        System.out.println(philosopher.getName() + " начал есть" + philosopher.getHunger());
        Thread.sleep(MIN_TIME_TO_EAT + (int) (Math.random() * (MAX_TIME_TO_EAT - MIN_TIME_TO_EAT)));
        System.out.println(philosopher.getName() + " поел");
        philosopher.eat();


        leftFork.putDown();
        Thread.sleep(MIN_TIME_TO_PUT_DOWN_FORK + (int) (Math.random() * (MAX_TIME_TO_PUT_DOWN_FORK - MIN_TIME_TO_PUT_DOWN_FORK)));
        System.out.println(philosopher.getName() + " положил левую вилку");

        rightFork.putDown();
        Thread.sleep(MIN_TIME_TO_PUT_DOWN_FORK + (int) (Math.random() * (MAX_TIME_TO_PUT_DOWN_FORK - MIN_TIME_TO_PUT_DOWN_FORK)));
        System.out.println(philosopher.getName() + " положил правую вилку");

        philosopher.hasMeal = false;
        leftPhilosopher.awaken();
        rightPhilosopher.awaken();
    }

    /**
     * класс голода философа
     */
    public class HungerThread implements Runnable {
        /**
         * объект философа
         */
        public Philosopher philosopher;

        /**
         * минимальное время в мс через которое прибавляется голод
         */
        public static final int MIN_TIME_TO_ADD_HUNGER = 500;

        /**
         * максимальное время в мс через которое прибавляется голод
         */
        public static final int MAX_TIME_TO_ADD_HUNGER = 1500;

        /**
         * констуктоор
         */
        public HungerThread() {
            this.philosopher = PhilosopherThread.this.philosopher;
        }

        /**
         * run метод от Runnable интерфейса
         */
        public void run() {
            try {
                while (true) {
                    Thread.sleep(MIN_TIME_TO_ADD_HUNGER + (int) (Math.random() * (MAX_TIME_TO_ADD_HUNGER - MIN_TIME_TO_ADD_HUNGER)));
                    philosopher.addHunger();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
