package task4;

/**
 * класс философа
 */
public class Philosopher {
    /** показатель голода */
    public int hunger = 0;

    /** имя философа */
    public String name;

    /** статус, того начался ли процесс трапезы,
     * который включает в себя взятие вилок, сам процесс поедания,
     * и возвращение вилок на стол */
    public boolean hasMeal = false;

    /** показатель голода, при котором философ голодает */
    public static final int MAX_HUNGER_TO_EXTREME_HUNGER = 15;

    /**
     * конструктор
     * @param name - имя философа
     */
    public Philosopher(String name) {
        this.name = name;
    }

    /**
     * getter для голода
     * @return показатель голода
     */
    public int getHunger() {
        return hunger;
    }

    /**
     * setter для голода
     * @param hunger - показатель голода
     */
    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    /**
     * getter для имени
     * @return - имя философа
     */
    public String getName() {
        return name;
    }

    /**
     * setter для имени
     * @param name - имя философа
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * метод добавления к показателю голода
     */
    public void addHunger() {
        this.hunger += 1;
    }

    /**
     * метод узнать голоден ли философ
     * @return
     */
    public boolean isHunger() {
        return this.hunger > MAX_HUNGER_TO_EXTREME_HUNGER;
    }

    /**
     * метод, чтобы поесть
     */
    public void eat() {
        setHunger(0);
    }

    /**
     * метод, чтобы размышлять
     * @throws InterruptedException
     */
    public synchronized void meditate() throws InterruptedException {
        wait();
    }

    /**
     * метод, чтобы пробудить философов
     */
    public synchronized void awaken() {
        notifyAll();
    }
}
