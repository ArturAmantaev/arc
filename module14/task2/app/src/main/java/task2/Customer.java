package task2;

import java.util.concurrent.Semaphore;

/**
 * класс клиента
 */
public class Customer implements Runnable {
    /** объект рабочего места */
    public Workplace workplace;

    /** объект ресепшен */
    public Reception reception;

    /** объект семафора */
    public Semaphore movement;

    /** статус клиента */
    public CustomerStatus status = CustomerStatus.CAME_TO_BARBERSHOP;

    /**
     * getter для статуса
     * @return CustomerStatus - статус клиента
     */
    public CustomerStatus getStatus() {
        return status;
    }

    /**
     * сеттер для статуса
     * @param status - статус клиента
     */
    public void setStatus(CustomerStatus status) {
        this.status = status;
    }

    /**
     * конструктор
     * @param workplace - объект рабочего места
     * @param reception - объект ресепшен
     * @param movement - объект семафора
     */
    public Customer(Workplace workplace, Reception reception, Semaphore movement) {
        this.workplace = workplace;
        this.reception = reception;
        this.movement = movement;
    }

    /**
     * run метод от Runnable интерфейса
     */
    public void run() {
        try {
            movement.acquire();
            System.out.println(Thread.currentThread().getName() + " пришел");
            switch (workplace.status) {
                case EMPTY:
                case BUSY_HAIRDRESSER: {
                    movement.release();
                    workplace.lock(this);
                    break;
                }
                case BUSY_CUSTOMER: {
                    System.out.println(Thread.currentThread().getName() + " идет на ресепшн");

                    movement.release();

                    if (reception.haveEmptyPlace()) {
                        reception.lock(this);
                        workplace.lock(this);
                    } else {
                        System.out.println(Thread.currentThread().getName() + " не нашел свободного места на ресепшн и пошел домой");
                    }
                    break;
                }
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
