package task2;

public class Workplace {
    /** статус рабочего места */
    public WorkplaceStatus status;

    /** объект клиента */
    public Customer customer;

    /**
     * setter для объекта клиента
     * @param customer - объект клиент
     */
    public synchronized void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * getter для объекта клиентов
     * @return объект клиента
     */
    public synchronized Customer getCustomer() {
        return customer;
    }

    /**
     * getter для статуса рабочего места
     * @return WorkplaceStatus - статус рабочего места
     */
    public synchronized WorkplaceStatus getStatus() {
        return status;
    }

    /**
     * setter для статуса рабочего места
     * @param status - WorkplaceStatus статус рабочего места
     */
    public synchronized void setStatus(WorkplaceStatus status) {
        this.status = status;
    }

    /** минимальное время стрижки в мс */
    public static final int MIN_WORKING_TIME = 5000;

    /** максимальное время стрижки в мс */
    public static final int MAX_WORKING_TIME = 10000;

    /**
     * метод блокировки рабочего места
     * @param customer - объект клиента
     * @throws InterruptedException
     */
    public synchronized void lock(Customer customer) throws InterruptedException {
        if (getStatus() == WorkplaceStatus.BUSY_HAIRDRESSER) {
            notifyAll();
            System.out.println(Thread.currentThread().getName() + " разудил парикмахера");
        }

        setCustomer(customer);
        customer.setStatus(CustomerStatus.SITTING_AT_WORKPLACE);
        setStatus(WorkplaceStatus.BUSY_CUSTOMER);
        System.out.println(Thread.currentThread().getName() + " сел в кресло");

        while (customer.getStatus() != CustomerStatus.GOT_HAIRCUT) {
            this.wait();
        }
        System.out.println(Thread.currentThread().getName() + " встал с кресла и пошел домой");
    }

    /**
     * метод занять кресло для парикмахера
     * @param hairdresser - объект парикмахера
     * @throws InterruptedException
     */
    public synchronized void lock(Hairdresser hairdresser) throws InterruptedException {
        while (getStatus() != WorkplaceStatus.BUSY_CUSTOMER) {
            setStatus(WorkplaceStatus.BUSY_HAIRDRESSER);
            hairdresser.setStatus(HairdresserStatus.SLEEP);
            System.out.println("Парикмахер уснул");
            this.wait();
        }

        System.out.println("Кто-то есть в кресле");
        work(hairdresser);
    }

    /**
     * метод, чтобы производить стрижку
     * @param hairdresser - объект парикмахера
     * @throws InterruptedException
     */
    public synchronized void work(Hairdresser hairdresser) throws InterruptedException {
        hairdresser.setStatus(HairdresserStatus.WORK);
        System.out.println("Начал стричь");

        Thread.sleep(MIN_WORKING_TIME + (int) (Math.random() * (MAX_WORKING_TIME - MIN_WORKING_TIME)));

        System.out.println("Закончил стрижку");

        setStatus(WorkplaceStatus.EMPTY);
        hairdresser.setStatus(HairdresserStatus.THINK);
        getCustomer().setStatus(CustomerStatus.GOT_HAIRCUT);

        this.notify();
    }
}
