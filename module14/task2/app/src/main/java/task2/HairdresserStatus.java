package task2;

/**
 * enum для статуса парикмахера
 */
public enum HairdresserStatus {
    SLEEP, WORK, CHECK, THINK
}
