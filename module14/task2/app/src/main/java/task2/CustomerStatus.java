package task2;

/**
 * enum для статуса клиента
 */
public enum CustomerStatus {
    CAME_TO_BARBERSHOP, SITTING_AT_RECEPTION, SITTING_AT_WORKPLACE, GOT_HAIRCUT
}
