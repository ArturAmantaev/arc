package task2;

import java.util.concurrent.Semaphore;

/**
 * класс парикмахера
 */
public class Hairdresser implements Runnable {
    /** объект рабочего места */
    public Workplace workplace;

    /** объект ресепшен */
    public Reception reception;

    /** объект семафора */
    public Semaphore movement;

    /** статус парикмахера */
    public HairdresserStatus status = HairdresserStatus.THINK;

    /** минимальное время проверки наличия клиентов на ресепшн */
    public static final int MIN_CHECK_TIME = 1000;

    /** максимальное время проверки наличия клиентов на ресепшн */
    public static final int MAX_CHECK_TIME = 1000;

    /**
     * getter для статуса
     * @return HairdresserStates - статус парикмахера
     */
    public HairdresserStatus getStatus() {
        return status;
    }

    /**
     * setter для статуса
     * @param status - статус парикмахера
     */
    public void setStatus(HairdresserStatus status) {
        this.status = status;
    }

    /**
     * конструктор
     * @param workplace - объект рабочего места
     * @param reception - объект ресепшен
     * @param movement - объект семафора
     */
    public Hairdresser(Workplace workplace, Reception reception, Semaphore movement) {
        this.workplace = workplace;
        this.reception = reception;
        this.movement = movement;
    }

    /**
     * run метод от Runnable интерфейса
     */
    public void run() {
        while (true) {
            try {
                movement.acquire();
                setStatus(HairdresserStatus.CHECK);

                System.out.println("Парикмахер занял мьютекс. Началась проверка клиентов");

                if (reception.getNumberClients() > 0) {
                    reception.check();
                }

                Thread.sleep(MIN_CHECK_TIME + (int) (Math.random() * (MAX_CHECK_TIME - MIN_CHECK_TIME)));

                System.out.println("Парикмахер освободил мьютекс. Проверка закончилась");

                movement.release();
                workplace.lock(this);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
