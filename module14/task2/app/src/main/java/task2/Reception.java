package task2;

import java.util.ArrayList;

/**
 * класс ресепшн
 */
public class Reception {
    /** количество клиентов */
    private static final ArrayList<Customer> customers = new ArrayList<Customer>();

    /** максимальное количество мест для ожидающих */
    public static final int MAX_PLACE = 5;

    /**
     * метод получения количества клиентов на ресепшн
     * @return int - количество клиентов
     */
    public synchronized static int getNumberClients() {
        return customers.size();
    }

    /**
     * метод добавления клиента в очередь
     * @param customer - клиент
     */
    public synchronized static void addCustomer(Customer customer) {
        customers.add(customer);
    }

    /**
     * метод удаления клиента из очереди
     * @param customer - клиент
     */
    public synchronized static void removeCustomer(Customer customer) {
        customers.remove(customer);
    }

    /**
     * метод получения списка клиентов в очереди
     * @return ArrayList - список клиентов
     */
    public synchronized ArrayList getCustomers() {
        return customers;
    }

    /**
     * метод занятия места
     * @throws InterruptedException
     */
    public void lock(Customer customer) throws InterruptedException {
        addCustomer(customer);
        customer.setStatus(CustomerStatus.SITTING_AT_RECEPTION);
        System.out.println(Thread.currentThread().getName() + " сел на ресепшн");

        wait();

        removeCustomer(customer);
        System.out.println(Thread.currentThread().getName() + " встал с ресепшн");
    }

    /**
     * метод пробуждения одного из очереди
     */
    public synchronized void check() {
        this.notify();
    }

    /**
     * метод проверки наличия свободных мест
     * @return boolean - есть или нет свободных мест
     */
    public boolean haveEmptyPlace() {
        return getCustomers().size() < MAX_PLACE;
    }
}
