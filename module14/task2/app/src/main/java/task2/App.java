package task2;

import java.util.concurrent.Semaphore;

/**
 * класс с main методом
 */
public class App {
    /** минимальное время между приходами посетителей */
    public static final int MIN_TIME_BETWEEN_VISITS = 5000;

    /** максимальное время между приходами посетителей */
    public static final int MAX_TIME_BETWEEN_VISITS = 10000;

    /**
     * main метод
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(1);
        Reception reception = new Reception();
        Workplace workplace = new Workplace();
        Thread hairdresserThread = new Thread(new Hairdresser(workplace, reception, semaphore));
        hairdresserThread.start();
        Thread.sleep(1000);
        int i = 0;
        while(true) {
            Thread customerThread = new Thread(new Customer(workplace, reception, semaphore), "Посетитель" + i);
            customerThread.start();
            Thread.sleep(MIN_TIME_BETWEEN_VISITS + (int) (Math.random() * (MAX_TIME_BETWEEN_VISITS - MIN_TIME_BETWEEN_VISITS)));
            i++;
        }
    }
}
