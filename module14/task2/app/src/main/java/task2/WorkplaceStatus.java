package task2;

/**
 * enum для статуса рабочего места
 */
public enum WorkplaceStatus {
    EMPTY, BUSY_HAIRDRESSER, BUSY_CUSTOMER
}
