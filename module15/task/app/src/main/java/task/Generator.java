package task;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Random;

/**
 * класс генератор
 */
public class Generator {

    /** размер генерированного отображения */
    public static int mapSize = 100_000;

    /** максимальная длина строки в отображении */
    public static int maxLengthString = 10;

    /**
     * метод генерации отображения
     * @param map - исходный Map, в который будет записано и возвращенно сгенерированное отображение
     * @return Map<Integer, MyObject> - отображение, наполненное значениями
     */
    public static Map<Integer, MyObject> generateMap(Map map) {
        for (int i = 0; i < mapSize; i++) {
            Object obj = new MyObject(generateNumber(Integer.MAX_VALUE), generateString(maxLengthString));
            map.put(generateNumber(Integer.MAX_VALUE), obj);
        }
        return map;
    }

    /**
     * метод генерации числа
     * @param maxNumber - предел генерации (не включительно)
     * @return Integer - сгенерированное число
     */
    public static Integer generateNumber(int maxNumber) {
        return (Integer) (int) (Math.random() * maxNumber);
    }

    /**
     * метод, генерирующий строку
     * @param length - длина сгенерированной строки
     * @return String - сгенерированная строка
     */
    public static String generateString(int length) {
        byte[] array = new byte[length];
        new Random().nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }
}
