package task;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * класс сериализатор
 */
public class Serializer {
    /**
     * метод для сериализации объекта в файл
     * @param path - путь к файлу
     * @param obj - объект, который необходимо сериализовать
     */
    public static void setByPath(String path, Object obj) {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(path))) {
            output.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
