package task;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * класс, содержащий main метод
 */
public class App {
    /**
     * main метод
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write 'get' or 'set'");
        String answer = sc.nextLine();
        System.out.println("Write file path");
        String path = sc.nextLine();
        switch (answer) {
            case "get": {
                Map map = Deserializer.getByPath(path);
                printMap(map);
                break;
            }
            case "set": {
                HashMap map = new HashMap();
                Serializer.setByPath(path, Generator.generateMap(map));
            }
        }
    }

    /**
     * метод вывода отображения в консоль
     * @param map - Map, который нужно вывести
     */
    public static void printMap(Map map) {
        map.forEach((key, value) -> System.out.println(key + ":" + value));
    }
}
