package task;

import java.io.Serializable;

/**
 * класс реализующий интерфейс Serializable, объекты которого будут находиться в отображении
 */
public class MyObject implements Serializable {
    /** Serial Version UID */
    private static final long serialVersionUID = -3319462319475040732L;

    /** числовое поле a */
    public int a;

    /** строковое поле b */
    public String b;

    /**
     * конструктор
     * @param a - int
     * @param b - String
     */
    public MyObject(int a, String b) {
        this.a = a;
        this.b = b;
    }

    /**
     * метод toString
     * @return String - строковое представление объекта
     */
    @Override
    public String toString() {
        return this.a + " " + this.b;
    }
}
