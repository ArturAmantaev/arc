package task;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

/**
 * класс десериализатор
 */
public class Deserializer {
    /**
     * метод десериализации объекта по пути к файлу
     * @param path - путь к файлу
     * @return Map<Integer, MyObject> - десериализованное отображение
     */
    public static Map getByPath(String path) {
        Map<Integer, MyObject> map = null;

        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(path))){
            map = (Map) input.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
