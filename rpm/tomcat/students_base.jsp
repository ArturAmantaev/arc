<%@ page contentType="text/html;charset=utf-8" %>
<%@ page import="students.decorators.StudentDecorator" %>
<jsp:useBean id="students" scope="request" type="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Таблица студентов</title>
</head>
<body>
<% for (int i = 0; i < students.size(); i++) { %>
<p>
    <%= ((StudentDecorator) students.get(i)).getId()%>
    <%= ((StudentDecorator) students.get(i)).getName()%>
    <%= ((StudentDecorator) students.get(i)).getGroupName()%>
    <%= ((StudentDecorator) students.get(i)).getRoomNumber()%>
</p>
<% } %>
</body>
</html>