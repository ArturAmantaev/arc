package students.decorators;

import students.models.Group;
import students.models.Room;
import students.models.Student;

import java.io.Serializable;

/**
 * класс декоратор для студента
 */
public class StudentDecorator extends Student implements Serializable {
    /** SERIAL VERSION UID */
    private static final long serialVersionUID = -461753413510519396L;

    /**
     * конструктор
     */
    public StudentDecorator() {}

    /**
     * конструктор
     * @param id - id студента
     * @param name - имя студента
     * @param groupId - id группы студента
     * @param roomId - id комнаты студента
     */
    public StudentDecorator(int id, String name, int groupId, int roomId) {
        this.id = id;
        this.groupId = groupId;
        this.name = name;
        this.roomId = roomId;
    }

    /**
     * конструктор
     * @param id - id студента
     * @param name - имя студента
     * @param room - комната студента
     * @param group - группа студента
     */
    public StudentDecorator(int id, String name, Room room, Group group) {
        this.id = id;
        this.groupId = group.getId();
        this.name = name;
        this.roomId = room.getId();
        this.room = room;
        this.group = group;
    }

    /**
     * колучение названия группы студента
     * @return String - название группы
     */
    public String getGroupName() {
        return this.getGroup().getName();
    }

    /**
     * получение номера комнаты студента
     * @return int - номер комнаты
     */
    public int getRoomNumber() {
        return this.getRoom().getNumber();
    }
}
