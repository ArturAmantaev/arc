package students.services;

import students.lib.sql.Connect;
import students.models.SerializedStudent;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * класс для десериализации
 */
public class Deserializer {
    /**
     * метод десериализации объекта из БД
     * @param id - id объекта, которого нужно десериализовать
     * @return Object - десериализованный объект
     * @throws SQLException
     */
    public static SerializedStudent deserializeFromDB(int id) throws SQLException {
        Object obj;
        String sql = "select obj from objtest where student_id = " + id + " limit 1";
        Statement statement = Connect.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultSet = statement.executeQuery(sql);
        ByteArrayInputStream baos;
        ObjectInputStream objectInputStream;
        byte[] binaryBuffer;

        try {
            resultSet.last();

            binaryBuffer = resultSet.getBytes("obj");
            baos = new ByteArrayInputStream(binaryBuffer, 0, binaryBuffer.length);
            objectInputStream = new ObjectInputStream(baos);
            obj = objectInputStream.readObject();

            objectInputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return (SerializedStudent) obj;
    }
}
