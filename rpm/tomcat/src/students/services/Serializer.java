package students.services;

import students.lib.sql.Connect;
import students.models.SerializedStudent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * класс для сериализации
 */
public class Serializer {
    /**
     * метод сериализации объекта в БД
     * @param id - id для идентификации сериализованных объектов
     * @param obj - объект, который нужно сериализовать
     * @throws Exception
     */
    public static void serializeToDBById(int id, SerializedStudent obj) throws Exception {
        String sql_base = "select * from objtest where (student_id = " + id + ")";
        ResultSet rs = Connect.execute(sql_base);
        if (rs.next()) {
            String sql = "UPDATE objtest SET obj = ? WHERE student_id = " + id;
            serializeObjectToDb(obj, sql);
        } else {
            String sql = "insert into objtest(student_id, obj) values(" + id + ", ?)";
            serializeObjectToDb(obj, sql);
        }
    }

    /**
     * метод сериализации объекта в БД по sql запросу
     * @param obj - объект, который нужно сериализовать
     * @param sql - строка запроса
     * @throws Exception
     */
    private static void serializeObjectToDb(SerializedStudent obj, String sql) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(obj);
        oos.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        PreparedStatement preparedStatement = Connect.getConnection().prepareStatement(sql);
        preparedStatement.setBinaryStream(1, bais);
        preparedStatement.executeUpdate();
    }
}
