package students.models;

import students.lib.BaseClass;

import java.io.Serializable;

/**
 * класс группы
 */
public class Group extends BaseClass implements Serializable {
    /** SERIAL VERSION UID */
    private static final long serialVersionUID = -3838925377300888200L;

    /** id группы */
    private int id;

    /** название группы */
    private String name;

    /**
     * конструктор
     */
    public Group() {
    }

    /**
     * конструктор
     * @param name - название группы
     */
    public Group(String name) {
        this.name = name;
    }

    /**
     * конструктор
     * @param id - id группы
     * @param name - название группы
     */
    public Group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * получение id группы
     * @return int - id группы
     */
    public int getId() {
        return id;
    }

    /**
     * установка id группы
     * @param id - id группы
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * получение названия группы
     * @return String - название группы
     */
    public String getName() {
        return name;
    }

    /**
     * установка названия группы
     * @param name - название группы
     */
    public void setName(String name) {
        this.name = name;
    }
}
