package students.models;

import students.decorators.StudentDecorator;
import students.services.Deserializer;
import students.services.Serializer;

import java.sql.SQLException;

/**
 * класс для сериализации студента
 */
public class SerializedStudent extends StudentDecorator implements SerializedObject{
    /**
     * метод сериализации студента в БД
     * @throws Exception
     */
    public void serialize() throws Exception {
        Serializer.serializeToDBById(id, this);
    }

    /**
     * метод десериализации студента из БД
     * @throws SQLException
     */
    public void deserialize() throws SQLException {
        Deserializer.deserializeFromDB(id);
    }
}
