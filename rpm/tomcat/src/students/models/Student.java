package students.models;

import students.lib.BaseClass;
import students.lib.sql.Connect;
import students.lib.sql.Extractor;
import students.lib.sql.Query;
import students.decorators.StudentDecorator;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * класс модели студента
 */
public class Student extends BaseClass implements Serializable {
    /** SERIAL VERSION UID */
    private static final long serialVersionUID = -4052278999457919797L;

    /** id студента */
    protected int id;

    /** id группы студента */
    protected int groupId;

    /** имя студента*/
    protected String name;

    /** id комнаты студента */
    protected int roomId;

    /** объект группы студента */
    protected Group group;

    /** объект комнаты студента */
    protected Room room;

    /** название таблицы в БД */
    protected static String tableName = "student";

    /**
     * конструктор
     */
    public Student() {
    }

    /**
     * конструктор
     * @param name - имя студента
     * @param groupId - id группы студента
     * @param roomId - id комнаты студента
     */
    public Student(String name, int groupId, int roomId) {
        this.name = name;
        this.groupId = groupId;
        this.roomId = roomId;
    }

    /**
     * конструктор
     * @param id - id студента
     * @param name - имя студента
     * @param groupId - id группы студента
     * @param roomId - id комнаты студента
     */
    public Student(int id, String name, int groupId, int roomId) {
        this.id = id;
        this.groupId = groupId;
        this.name = name;
        this.roomId = roomId;
    }

    /**
     * конструктор
     * @param id - id студента
     * @param name - имя студента
     * @param room - комната студента
     * @param group - группа студента
     */
    public Student(int id, String name, Room room, Group group) {
        this.id = id;
        this.groupId = group.getId();
        this.name = name;
        this.roomId = room.getId();
        this.room = room;
        this.group = group;
    }

    /**
     * конструктор
     * @param name - имя студента
     * @param room - комната студента
     * @param group - группа студента
     */
    public Student(String name, Room room, Group group) {
        this.groupId = group.getId();
        this.name = name;
        this.roomId = room.getId();
        this.room = room;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int id_group) {
        this.groupId = id_group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    /**
     * получение списка всех студентов из БД
     * @return ArrayList<StudentDdecorator> - список студентов
     * @throws SQLException
     */
    public static ArrayList<StudentDecorator> getAll() throws SQLException {
        Query query = new Query(
                "SELECT\n" +
                "    students.id id,\n" +
                "    students.name student_name,\n" +
                "    students.group_id group_id,\n" +
                "    students.room_id room_id,\n" +
                "    (SELECT groups.name FROM groups WHERE students.group_id = groups.id) group_name,\n" +
                "    (SELECT rooms.number FROM rooms WHERE students.room_id = rooms.id) room_number\n" +
                "FROM students;"
        );
        return execute(query, (Extractor<StudentDecorator>) rs -> {
            int studentId = rs.getInt("id");
            int roomId = rs.getInt("room_id");
            int groupId = rs.getInt("group_id");
            String groupName = rs.getString("group_name");
            String studentName = rs.getString("student_name");
            int roomNumber = rs.getInt("room_number");
            return new StudentDecorator(studentId, studentName, new Room(roomId, roomNumber), new Group(groupId, groupName));
        });
    }

    /**
     * получение студента из БД по id
     * @param id - id студента, которого нужно найти
     * @return StudentDecorator - найденный студент
     * @throws Exception
     */
    public static StudentDecorator getStudentById(int id) throws Exception {
        Query query = new Query(
                "SELECT\n" +
                "    students.id id,\n" +
                "    students.name student_name,\n" +
                "    students.group_id group_id,\n" +
                "    students.room_id room_id,\n" +
                "    (SELECT groups.name FROM groups WHERE students.group_id = groups.id) group_name,\n" +
                "    (SELECT rooms.number FROM rooms WHERE students.room_id = rooms.id) room_number\n" +
                "FROM students\n" +
                "WHERE id = " + id + ";"
        );
        ArrayList<StudentDecorator> resultArrayList = execute(query, (Extractor<StudentDecorator>) rs -> {
            int studentId = rs.getInt("id");
            int roomId = rs.getInt("room_id");
            int groupId = rs.getInt("group_id");
            String groupName = rs.getString("group_name");
            String studentName = rs.getString("student_name");
            int roomNumber = rs.getInt("room_number");
            return new StudentDecorator(studentId, studentName, new Room(roomId, roomNumber), new Group(groupId, groupName));
        });
        return resultArrayList.get(0);
    }

    /**
     * проверка студента на валидность
     * @return true или false
     */
    public boolean validate() {
        return name != null && (roomId != 0 || room != null) && (groupId != 0 || group != null);
    }

    /**
     * сохранение студента в БД
     * @param student - студент, которого надо сохранить
     * @throws SQLException
     */
    public static void create(Student student) throws SQLException {
        if (!student.validate()) throw new SQLException();
        String query =
                "insert into students (group_id, room_id, name)\n" +
                "select (select groups.id from groups where groups.name = '" + student.getGroup().getName() + "') group_id,\n" +
                "       (select rooms.id from rooms where rooms.number = " + student.getRoom().getNumber() + ") room_id,\n" +
                "       '" + student.getName() + "';";
        Connect.execute(query);
        fetchAfter(student);
    }

    /**
     * проверка после изменения
     * @param student - студент, наличие которого в БД надо проверить
     * @return int - id найденного студента
     * @throws SQLException
     */
    private static int fetchAfter(Student student) throws SQLException {
        ArrayList<String> conditions = new ArrayList<>();
        conditions.add("name = '" + student.getName() + "'");
        conditions.add("group_id in (select groups.id from groups where groups.name = '" + student.getGroup().getName() + "')");
        conditions.add("room_id in (select rooms.id from rooms where rooms.number = " + student.getRoom().getNumber() + ")");
        Query query = new Query("select id from students").where(conditions);

        int id = (int) execute(query, (Extractor<Integer>) rs -> rs.getInt("id")).get(0);

        if (id == 0) throw new SQLException(String.valueOf(id));
        return id;
    }

    /**
     * удаление студента по id
     * @param id - id студента, которого надо удалить
     * @throws Exception
     */
    public static void deleteById(int id) throws Exception {
        getStudentById(id);

        Map<String, String> map = new HashMap<>();
        map.put("id", " = " + id);
        deleteBy(map, tableName);

        try {
            getStudentById(id);
        } catch (Exception e) {
            return;
        }
        throw new SQLException();
    }

    /**
     * обновить запись о студенте
     * @param id - id студента, которого надо обновить
     * @param name - новое имя студента
     * @param groupName - новое название группы студента
     * @param roomNumber - новый номер комнаты студента
     * @throws Exception
     */
    public static void updateById(int id, String name, String groupName, int roomNumber) throws Exception {
        getStudentById(id);
        Map<String, String> map = new HashMap<>();
        map.put("name", "'" + name + "'");
        map.put("group_id", groupName);
        map.put("room_id", String.valueOf(roomNumber));

        Connect.execute(Query.updateBy(map, tableName).where("id = " + id).toString());

        if (id != fetchAfter(new Student(id, name, new Room(roomNumber), new Group(groupName))))
            throw new SQLException();
    }

    /**
     * получение списка студентов по названию группы
     * @param groupName - название группа
     * @return ArrayList<StudentDecorator> - список студентов
     * @throws SQLException
     */
    public static ArrayList<StudentDecorator> getStudentByGroupName(String groupName) throws SQLException {
        Query query = new Query(
                "SELECT\n" +
                "    students.id id,\n" +
                "    students.name student_name,\n" +
                "    (SELECT rooms.number FROM rooms WHERE students.room_id = rooms.id) room_number,\n" +
                "    (SELECT groups.name FROM groups WHERE groups.id = students.group_id AND groups.name = '" + groupName + "') group_name\n" +
                "FROM students;\n"
        );
        return execute(query, (Extractor<StudentDecorator>) rs -> {
            int studentId = rs.getInt("id");
            String studentName = rs.getString("student_name");
            int roomNumber = rs.getInt("room_number");
            return new StudentDecorator(studentId, studentName, new Room(roomNumber), new Group(groupName));
        });
    }
}
