package students.models;

/**
 * интерфейс для сериализованных обхектов
 */
public interface SerializedObject {
    /**
     * метод сериализации
     * @throws Exception
     */
    void serialize() throws Exception;

    /**
     * метод десериализации
     * @throws Exception
     */
    void deserialize() throws Exception;
}
