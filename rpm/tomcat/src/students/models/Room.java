package students.models;

import students.lib.BaseClass;

import java.io.Serializable;

/**
 * класс комнаты
 */
public class Room extends BaseClass implements Serializable {
    /** SERIAL VERSION UID */
    private static final long serialVersionUID = -594217901045456239L;

    /** id комнаты */
    private int id;

    /** номер комнаты */
    private int number;

    /**
     * конструктор
     */
    public Room() {
    }

    /**
     * конструктор
     * @param number - номер комнаты
     */
    public Room(int number) {
        this.number = number;
    }

    /**
     * конструктор
     * @param id - id комнаты
     * @param number - номер комнаты
     */
    public Room(int id, int number) {
        this.id = id;
        this.number = number;
    }

    /**
     * получение id комнаты
     * @return int - id комнаты
     */
    public int getId() {
        return id;
    }

    /**
     * установка id комнаты
     * @param id - id комнаты
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * получение номера комнаты
     * @return - номер комнаты
     */
    public int getNumber() {
        return number;
    }

    /**
     * установка номера комнаты
     * @param number - номер комнаты
     */
    public void setNumber(int number) {
        this.number = number;
    }
}
