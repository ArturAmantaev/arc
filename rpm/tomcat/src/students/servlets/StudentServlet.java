package students.servlets;

import students.decorators.StudentDecorator;
import students.lib.helpers.MyHttpHelper;
import students.models.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

/**
 * класс сервлет для студантов, по адресу '.../students/<id>'
 */
@WebServlet("/students/*")
public class StudentServlet extends HttpServlet {

    /**
     * метод, обрабатываающий GET запрос, отображающий студента с необходимым id, хранящегося в БД
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            String id = request.getPathInfo().substring(1);
            StudentDecorator student = Student.getStudentById(Integer.parseInt(id));
            request.setAttribute("student", student);
            getServletContext().getRequestDispatcher("/student_base.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            response.setStatus(422);
            e.printStackTrace();
        }
    }

    /**
     * метод, обрабатывающий DELETE запрос, удаляющий студента с необходимым id, хранящегося в БД
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getPathInfo().substring(1));
            Student.deleteById(id);
        } catch (Exception e) {
            response.setStatus(422);
            PrintWriter out = response.getWriter();
            out.println(e);
        }
    }

    /**
     * метод обрабатывающий PUT запрос, обновляющий студента с заданным id, хранящегося в БД. Параметры берутся из body
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getPathInfo().substring(1));
        String body = MyHttpHelper.getBody(request, response);
        try {
            String name = MyHttpHelper.getNameFromBody(request, response, body);
            String groupName = MyHttpHelper.getGroupNameFromBody(request, response, body);
            int roomNumber = MyHttpHelper.getRoomNumberFromBody(request, response, body);
            Student.updateById(id, name, groupName, roomNumber);
        } catch (Exception e) {
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            response.setStatus(422);
            e.printStackTrace();
        }
    }

}
