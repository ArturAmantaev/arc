package students.servlets;

import students.decorators.StudentDecorator;
import students.models.SerializedStudent;
import students.models.Student;
import students.services.Deserializer;
import students.services.Serializer;

import javax.management.AttributeNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * класс сервлет для сериализации
 */
@WebServlet("/students_serializer/*")
public class StudentSerializerServlet extends HttpServlet {

    /**
     * метод, обрабатывающий GET запрос и отображающий
     * либо сериализованного студента при наличии его в таблице сериализованных объектов, либо студента из таблицы студентов
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            String idStr = request.getPathInfo().substring(1);
            if (idStr == null) throw new AttributeNotFoundException();

            int id = Integer.parseInt(idStr);

            StudentDecorator student;
            try {
                student = Deserializer.deserializeFromDB(id);
            } catch (Exception e) {
                student = StudentDecorator.getStudentById(id);
            }
            request.setAttribute("student", student);
            getServletContext().getRequestDispatcher("/student_base.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            response.setStatus(422);
            e.printStackTrace();
        }
    }

    /**
     * метод, обрабатывающий POST запрос для сериализации студента по id
     * @param request
     * @param response
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idStr = request.getPathInfo().substring(1);
            if (idStr == null) throw new AttributeNotFoundException();

            int id = Integer.parseInt(idStr);
            StudentDecorator student = Student.getStudentById(id);
            Serializer.serializeToDBById(id, (SerializedStudent) student);
        } catch (Exception e) {
        }
    }
}
