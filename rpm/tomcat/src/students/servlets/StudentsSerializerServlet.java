package students.servlets;

import students.models.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * класс сервлет для просмотра таблицы студентов с кнопками
 */
@WebServlet("/students_serialized")
public class StudentsSerializerServlet extends HttpServlet {

    /**
     * метод, обрабатывающий GET запрос и отображающий таблицу студентов с кнопками сериализации и десериализации
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        ArrayList students = null;
        try {
            students = Student.getAll();
        } catch (SQLException e) {
            response.setStatus(500);
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            throw new RuntimeException(e);
        }
        request.setAttribute("students", students);
        getServletContext().getRequestDispatcher("/students_serializer.jsp").forward(request, response);
    }
}
