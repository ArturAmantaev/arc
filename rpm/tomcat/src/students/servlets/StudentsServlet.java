package students.servlets;

import students.lib.helpers.MyHttpHelper;
import students.models.*;
import students.models.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * класс сервлет для студантов, по адресу '.../students'
 */
@WebServlet("/students")
public class StudentsServlet extends HttpServlet {

    /**
     * метод, обрабатываающий GET запрос, отображающий список всех студентов, хранящихся в БД
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        ArrayList students = null;
        try {
            students = Student.getAll();
        } catch (SQLException e) {
            response.setStatus(500);
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            throw new RuntimeException(e);
        }
        request.setAttribute("students", students);
        getServletContext().getRequestDispatcher("/students_base.jsp").forward(request, response);
    }


    /**
     * метод, обрабатывающий POST запрос для создания нового студента. Поля для создания студента передаются в формате form-data в body запроса
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String body = MyHttpHelper.getBody(request, response);

        try {
            String name = MyHttpHelper.getNameFromBody(request, response, body);
            String groupName = MyHttpHelper.getGroupNameFromBody(request, response, body);
            int roomNumber = MyHttpHelper.getRoomNumberFromBody(request, response, body);

            Student student = new Student(name, new Room(roomNumber), new Group(groupName));

            Student.create(student);
        } catch (Exception e) {
            response.setStatus(422);
            request.setAttribute("error", e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            e.printStackTrace();
        }
    }
}
