package students.servlets;

import students.decorators.StudentDecorator;
import students.models.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * класс сервлет для отображения отчета
 */
@WebServlet("/report")
public class ReportServlet extends HttpServlet {
//    private static String message = "Error during Servlet processing";

    /**
     * метод, обрабатывающий GET запрос и отображающий список студентов по заданным параметрам
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String groupName = request.getParameter("group_name");
        ArrayList<StudentDecorator> students = null;

        try {
            students = Student.getStudentByGroupName(groupName);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        request.setAttribute("students", students.toArray(new StudentDecorator[0]));
        getServletContext().getRequestDispatcher("/report.jsp").forward(request, response);
    }
}
