package students.lib.sql;

import java.util.ArrayList;
import java.util.Map;

/**
 * класс запроса
 */
public class Query {

    /** строка запроса */
    public String query;

    /**
     * конструктор
     * @param query - строка запроса
     */
    public Query(String query) {
        this.query = query;
    }

    /**
     * получение всех записей из таблицы в БД
     * @param map - поля, получаемые из БД
     * @param tableName - название таблицы
     * @return Query - запрос
     */
    public static Query getAll(Map<String, String> map, String tableName) {
        String[] fields = map.keySet().toArray(new String[0]);
        String query = "select ";
        for (int i = 0; i < fields.length - 1; i++) {
            query += fields[i] + " " + map.get(fields[i]) + ", ";
        }
        query += fields[fields.length - 1] + " " + map.get(fields[fields.length - 1]);
        query += " from \"" + tableName + "\"";
        return new Query(query);
    }

    /**
     * получение всех записей из таблицы В БД
     * @param strings - массив строк (полей)
     * @param tableName - название таблицы
     * @return Query - запрос
     */
    public static Query getAll(String[] strings, String tableName) {
        String query = "select ";
        for (int i = 0; i < strings.length - 1; i++) {
            query += strings[i] + ", ";
        }
        query += strings[strings.length - 1];
        query += " from \"" + tableName + "\"";
        return new Query(query);
    }

    /**
     * получение запроса на удаление всех записей из таблицы в БД
     * @param fields - поля и условия, по которым нужно удалить запись
     * @param tableName - название таблицы
     * @return String - строка запроса
     */
    public static String deleteBy(Map<String, String> fields, String tableName) {
        String[] keys = fields.keySet().toArray(new String[0]);
        String query = "delete from \"" + tableName + "s\" where ";
        for (int i = 0; i < keys.length - 1; i++) {
            String key = keys[i];
            String condition = fields.get(key);
            query += key + condition + " and ";
        }
        String lastKey = keys[keys.length - 1];
        String lastCondition = fields.get(lastKey);
        query += lastKey + lastCondition;
        return query;
    }

    /**
     * JOIN к запросу
     * @param table - название присоединяемой таблицы
     * @param condition - условие, по которому производится присоединение
     * @return Query - запрос
     */
    public Query join(String table, String condition) {
        return new Query(query + " join \"" + table + "\" on " + condition);
    }

    /**
     * WHERE к запросу
     * @param condition - условие
     * @return Query - запрос
     */
    public Query where(String condition) {
        this.query += " where " + condition;
        return this;
    }

    /**
     * WHERE к запросу
     * @param condition - список условий
     * @return Query - запрос
     */
    public Query where(ArrayList condition) {
        this.query += " where ";
        for (int i = 0; i < condition.size() - 1; i++) {
            this.query += condition.get(i) + " and ";
        }
        this.query += condition.get(condition.size() - 1);
        return this;
    }

    /**
     * обновление записи в таблице в БД
     * @param conditions - условия обновления
     * @param tableName - название таблицы
     * @return Query - запрос
     */
    public static Query updateBy(Map<String, String> conditions, String tableName) {
        String query = "update \"" + tableName + "s\" set ";
        String[] fields = conditions.keySet().toArray(new String[0]);
        for (int i = 0; i < fields.length - 1; i++) {
            query += fields[i] + " = " + conditions.get(fields[i]) + ", ";
        }
        query += fields[fields.length - 1] + " = " + conditions.get(fields[fields.length - 1]);
        return new Query(query);
    }

    /**
     * переопределение метода toString()
     * @return String - запрос
     */
    @Override
    public String toString() {
        return query;
    }
}
