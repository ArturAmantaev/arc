package students.lib.sql;

import java.io.*;
import java.sql.*;
import java.util.Properties;

/**
 * класс для подключения к БД
 */
public class Connect {

    /** имя пользователся для подключения к контейнеру */
    public static String userName = "postgres";

    /** пароль от пользователя для подключения к контейнеру*/
    public static String password = "123";

    /** url, по которому можно обратиться к контейнеру */
    public static String url = "jdbc:postgresql://127.0.0.1:5432/student-timetable";

    /** объект коннекта */
    public static Connection connection = null;

    /**
     * получение объекта коннекта или создание, при его пустоте
     * @return Connection
     */
    public static Connection getConnection() {
        if (connection == null) createConnection();
        return connection;
    }

    /**
     * создание коннекта
     */
    public static void createConnection() {
        Properties connectionProps = new Properties();
        connectionProps.put("user", userName);
        connectionProps.put("password", password);
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, connectionProps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection = conn;
    }

    /**
     * выполнение SQL запроса
     * @param query - строка запроса
     * @return ResultSet
     */
    public static ResultSet execute(String query) {
        Connection conn = Connect.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
}
