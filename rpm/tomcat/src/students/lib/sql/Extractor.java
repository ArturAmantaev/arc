package students.lib.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * интерфейс для лямбда функции
 * @param <T>
 */
public interface Extractor<T> {
    
    /**
     * метод для лямюда функции
     * @param rs - ResultSet
     * @return
     * @throws SQLException
     */
    public T extract(ResultSet rs) throws SQLException;
}
