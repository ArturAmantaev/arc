package students.lib;

import students.lib.sql.Connect;
import students.lib.sql.Extractor;
import students.lib.sql.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * базовый класс модели
 */
public class BaseClass {

    /**
     * выполнение запроса в БД
     * @param query - запрос
     * @param extractor - лямбда функция, для получения результата
     * @return ArrayList - список, заполненный объектами того, что возвращает лямбда
     * @throws SQLException
     */
    public static ArrayList execute(Query query, Extractor extractor) throws SQLException {
        ResultSet rs = Connect.execute(query.toString());
        ArrayList result = new ArrayList();
        while (rs.next()) {
            result.add(extractor.extract(rs));
        }
        return result;
    }

    /**
     * создание новой записи в таблице в БД
     * @param fields - условия создания
     * @param tableName - название таблицы в БД
     */
    public static void create(Map<String, String> fields, String tableName) {
        String[] keys = fields.keySet().toArray(new String[0]);
        String[] values = new String[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            values[i] = fields.get(keys[i]);
        }

        String query = "insert into \"" + tableName +"s\" (";
        query += String.join(", ", keys);
        query += ") values (";
        query += String.join(", ", values);
        query += ")";
        Connect.execute(query);
    }

    /**
     * удаление записи из таблицы в БД
     * @param fields - условия удаления
     * @param tableName - название таблицы
     */
    public static void deleteBy(Map<String, String> fields, String tableName) {
        Connect.execute(Query.deleteBy(fields, tableName));
    }
}
