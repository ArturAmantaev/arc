package students.lib.helpers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * класс помощник с http
 */
public class MyHttpHelper {

    /**
     * получить id, содержащийся в body запроса
     * @param request
     * @param response
     * @param body - body запроса
     * @return int - id из body
     * @throws IOException
     * @throws SQLException
     */
    public static int getIdFromBody(HttpServletRequest request, HttpServletResponse response, String body)
            throws IOException, SQLException {
        try {
            return Integer.parseInt(getSmthFromBody(request, response, body, "id"));
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        }
    }

    /**
     * получить name, содержащийся в body запроса
     * @param request
     * @param response
     * @param body - body запроса
     * @return String - name из body
     * @throws IOException
     * @throws SQLException
     */
    public static String getNameFromBody(HttpServletRequest request, HttpServletResponse response, String body)
            throws IOException, SQLException {
        return getSmthFromBody(request, response, body, "name");
    }

    /**
     * получить group_name, содержащийся в body запроса
     * @param request
     * @param response
     * @param body - body запроса
     * @return String - group_name из body
     * @throws IOException
     * @throws SQLException
     */
    public static String getGroupNameFromBody(HttpServletRequest request, HttpServletResponse response, String body)
            throws IOException, SQLException {
        try {
            return getSmthFromBody(request, response, body, "group_name");
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        }
    }

    /**
     * получить room_number, содержащийся в body запроса
     * @param request
     * @param response
     * @param body - body запроса
     * @return int - room_number из body
     * @throws IOException
     * @throws SQLException
     */
    public static int getRoomNumberFromBody(HttpServletRequest request, HttpServletResponse response, String body)
            throws IOException, SQLException {
        try {
            return Integer.parseInt(getSmthFromBody(request, response, body, "room_number"));
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        }
    }

    /**
     * получить что-либо, содержащееся в body запроса
     * @param request
     * @param response
     * @param body - body запроса
     * @param smth - строка, содержащее то, что необходимо найти
     * @return String
     * @throws IOException
     * @throws SQLException
     */
    private static String getSmthFromBody(HttpServletRequest request, HttpServletResponse response,
                                          String body, String smth)
            throws IOException, SQLException {
        if (body == null) throw new SQLException();
        Pattern pattern = Pattern.compile(smth + "\"\\s+([^-]+)", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(body);
        if (!matcher.find()) throw new SQLException();
        String result = matcher.group(1).trim();
        return result;
    }

    /**
     * получить body из запроса
     * @param request
     * @param resp
     * @return String - body запроса
     * @throws IOException
     */
    public static String getBody(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = request.getReader();
            char[] charBuffer = new char[128];
            int bytesRead = -1;
            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } catch (IOException ex) {
            resp.setStatus(500);
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    resp.setStatus(500);
                    throw ex;
                }
            }
        }
        return stringBuilder.toString();
    }
}
