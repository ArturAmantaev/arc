<%@ page import="students.decorators.StudentDecorator" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Отчет</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/report" method="get">
    <% String groupName = request.getParameter("group_name"); %>
    group_name: <input name="group_name" required value=<%= groupName != null ? groupName : "" %>>
    <br><br>
    <input type="submit" value="Submit"/>
</form>
<% StudentDecorator[] students = (StudentDecorator[]) request.getAttribute("students");%>
<% for (int i = 0; i < students.length; i++) { %>
<p>
    <%= students[i].getId()%>
    <%= students[i].getName()%>
    <%= students[i].getGroupName()%>
</p>
<% } %>
</body>
</html>