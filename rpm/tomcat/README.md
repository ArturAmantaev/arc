**Инструкция по установке**

1. Прочитать документация *https://www3.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html*
2. Установить *tomcat*
3. Скачать *postgresql-42.3.3.jar* по ссылку *https://jdbc.postgresql.org/download.html*
4. Скачать файлы из папки *src*
5. В apache-tomcat-8.5.76/webapps создать папку <название новой папки>. В ней создать еще папки /WEB-INF/classes
6. В созданную папку переместить 5 скачанных *.java* файла
7. Файлы error.jsp и students_base.jsp переместить в apache-tomcat-8.5.76/webapps/<название созданной папки>
8. Находясь в папке classes выполнить команды: javac -cp .:/<путь до твоего томката>/apache-tomcat-8.5.76/lib/servlet-api.jar:./<путь до томката>/apache-tomcat-8.5.76/lib/postgresql-42.3.3.jar *.java
9. Находясь в *apache-tomcat-8.5.76/bin* выполнить команду *bash catalina.sh run*
10. Не останавливая этот процесс открыть браузер
11. Перейти по ссылку **localhost:8080/<название созданной папки>/holders**

Примечание:
В файле Connect.java прописаны *userName* и *userPassword* - имя и пароль от docker контейнера