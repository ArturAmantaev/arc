<%@ page contentType="text/html;charset=utf-8" %>
<jsp:useBean id="student" scope="request" type="students.decorators.StudentDecorator"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Студент</title>
</head>
<body>
<p>
    <%=student.getId()%>
    <%=student.getName()%>
    <%=student.getGroupName()%>
</p>
</body>
</html>