<%@ page contentType="text/html;charset=utf-8" %>
<%@ page import="students.decorators.StudentDecorator" %>
<jsp:useBean id="students" scope="request" type="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Таблица студентов</title>
</head>
<body>
<table border="1">
    <caption>Таблица студентов</caption>
    <tr>
        <th>Id студента</th>
        <th>Имя</th>
        <th>Группа</th>
        <th>Номер комнаты</th>
    </tr>
    <% for (int i = 0; i < students.size(); i++) { %>
    <tr>
        <td><%= ((StudentDecorator) students.get(i)).getId()%>
        </td>
        <td><%= ((StudentDecorator) students.get(i)).getName()%>
        </td>
        <td><%= ((StudentDecorator) students.get(i)).getGroupName()%>
        </td>
        <td><%= ((StudentDecorator) students.get(i)).getRoomNumber()%>
        </td>
        <td><p>
            <form action="${pageContext.request.contextPath}/students_serialized/<%=((StudentDecorator) students.get(i)).getId()%>" method="post">
                <button type="submit">Сериализовать
                </button>
            </form>
        </td>
        <td><p>
            <form action="${pageContext.request.contextPath}/students_serialized/<%=((StudentDecorator) students.get(i)).getId()%>" method="get">
                <button type="submit">Посмотреть содержимое
                </button>
            </form>
        </td>
    </tr>
    <% } %>
</table>
</body>
</html>