package ru.sirius.module07;

import java.util.Scanner;

class SortJ {
public static void main(String[] args) {
    int num_elements;
    Scanner sc = new Scanner(System.in);
    num_elements = sc.nextInt();
    int[] a = new int[num_elements];
    for (int i = 0 ; i < num_elements; i++) {
        a[i] = sc.nextInt();
    }
    int num_sorted = 0;
    while (num_sorted!=num_elements-1) {
       num_sorted = 0;
       int t;
       for(int i = 0 ; i < num_elements - 1 ; i++) {
           if(a[i] > a[i+1]) {
              t = a[i];
              a[i] = a[i+1] ;
              a[i+1] = t;
           }
	   else {
	      num_sorted++;
	   }
        }
    }
    for(int i = 0 ; i < num_elements; i++) {
        System.out.print(a[i]+" ");
    }
}
}
