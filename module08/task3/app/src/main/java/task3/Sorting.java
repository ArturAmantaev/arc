package task3;

class Sorting {

    public static void sort(double[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    double t = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = t;
                }
            }
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }

    public static double[] createSortedArray(double[] a) {
        double[] b = a.clone();
        int n = b.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (b[j] > b[j + 1]) {
                    double t = b[j];
                    b[j] = b[j + 1];
                    b[j + 1] = t;
                }
            }
        }
        return b;
    }
}
