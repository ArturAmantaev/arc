package task1;

import java.math.BigInteger;
import java.util.Scanner;

class Fibonachi {
    public static String fibonachi(long n) {
        String res = "";
        BigInteger first = BigInteger.valueOf(1);
        BigInteger second = BigInteger.valueOf(0);
        long fibend = n + 1;
        if (fibend == 2) {
            res += second + " ";
        } else if (fibend > 1) {
            res += second + " ";
        }
        for (int i = 0; i < fibend - 2; i++) {
            BigInteger result = first.add(second);
            res += result + " ";
            first = second;
            second = result;
        }
        return res;
    }
}
