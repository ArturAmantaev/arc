package task1;

import java.math.BigInteger;

class FibonachiWithRe {

    BigInteger[] array;
    boolean flag = true;

    public BigInteger fibonachi(BigInteger n) {
        if (this.flag) {
            this.flag = false;
            this.array = new BigInteger[n.intValue()];
            this.array[0] = BigInteger.ZERO;
            if (n.compareTo(BigInteger.ONE) > 0) {
                this.array[1] = BigInteger.ONE;
                if (n.compareTo(BigInteger.TWO) > 0) {
                    this.array[2] = BigInteger.ONE;
                }
            }
            for (int i = 3; i < n.intValue(); i++) {
                this.array[i] = BigInteger.ZERO;
            }
        }
        BigInteger sum = BigInteger.ZERO;
        if (n.intValue() == 1) {
            return this.array[0];
        } else if (n.intValue() == 2) {
            return this.array[1];
        } else {
            if ((this.array[n.intValue() - 1]).compareTo(BigInteger.ZERO) == 0) {
                this.array[n.intValue() - 1] = fibonachi(n.subtract(BigInteger.ONE));
            }
            if (this.array[n.intValue() - 2].compareTo(BigInteger.ZERO) == 0) {
                this.array[n.intValue() - 2] = fibonachi(n.subtract(BigInteger.TWO));
            }
            sum = sum.add(this.array[n.intValue() - 1]);
            sum = sum.add(this.array[n.intValue() - 2]);
            return sum;
        }
    }
}
