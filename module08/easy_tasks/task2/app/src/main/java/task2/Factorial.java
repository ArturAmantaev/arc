package task2;

import java.math.BigInteger;

class Factorial {
    public static BigInteger factorial(BigInteger n) {
        if (n.compareTo(BigInteger.valueOf(0)) > 0) {
            BigInteger f = BigInteger.valueOf(1);
            BigInteger i = BigInteger.valueOf(1);
            while (i.compareTo(n) == -1) {
                f = f.multiply(i);
                i = i.add(BigInteger.valueOf(1));
            }
            return f.multiply(n);
        } else if (n.compareTo(BigInteger.valueOf(0)) == 0) {
            return BigInteger.ONE;
        }
        else {
            return BigInteger.ZERO;
        }
    }
}
