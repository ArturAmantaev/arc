package task2;

import java.math.BigInteger;
import java.util.*;

class FactorialWithRe {
    private static BigInteger factorialRe(BigInteger value) {
        BigInteger total = BigInteger.ONE;
        for (int i = 0; value.compareTo(BigInteger.ONE) == 1; i++) {
            total = total.multiply(value);
            value = value.subtract(BigInteger.ONE);
        }
        return total;
    }

    public static BigInteger factorial(BigInteger n) {
        if (n.compareTo(BigInteger.valueOf(0)) > 0) {
            return factorialRe(n);
        } else if (n.compareTo(BigInteger.valueOf(0)) == 0) {
            return BigInteger.ONE;
        } else {
            return BigInteger.ZERO;
        }
    }
}
