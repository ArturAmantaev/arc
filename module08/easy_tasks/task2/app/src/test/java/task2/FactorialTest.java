package task2;

import org.junit.jupiter.api.Test;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactorialTest {
    @Test void factorial() {
        BigInteger n1 = BigInteger.valueOf(11);
        BigInteger n2 = BigInteger.valueOf(39916800);
        assertEquals(n2, Factorial.factorial(n1));
    }

    @Test void factorialOne() {
        BigInteger n1 = BigInteger.valueOf(1);
        BigInteger n2 = BigInteger.valueOf(1);
        assertEquals(n2, Factorial.factorial(n1));
    }

    @Test void factorialNull() {
        BigInteger n1 = BigInteger.valueOf(0);
        BigInteger n2 = BigInteger.valueOf(1);
        assertEquals(n2, Factorial.factorial(n1));
    }

    @Test void factorialRe() {
        BigInteger n1 = BigInteger.valueOf(11);
        BigInteger n2 = BigInteger.valueOf(39916800);
        assertEquals(n2, FactorialWithRe.factorial(n1));
    }

    @Test void factorialReOne() {
        BigInteger n1 = BigInteger.valueOf(1);
        BigInteger n2 = BigInteger.valueOf(1);
        assertEquals(n2, FactorialWithRe.factorial(n1));
    }

    @Test void factorialReNull() {
        BigInteger n1 = BigInteger.valueOf(0);
        BigInteger n2 = BigInteger.valueOf(1);
        assertEquals(n2, FactorialWithRe.factorial(n1));
    }
}
