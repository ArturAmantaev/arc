package task3;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumNTest {
    @Test void sumN() {
        BigInteger n1 = BigInteger.valueOf(1000);
        BigInteger n2 = BigInteger.valueOf(500000);
        assertEquals(n2, SumN.sum(n1));
    }

    @Test void sumNOne() {
        BigInteger n1 = BigInteger.ONE;
        BigInteger n2 = BigInteger.ONE;
        assertEquals(n2, SumN.sum(n1));
    }

    @Test void sumNZero() {
        BigInteger n1 = BigInteger.ZERO;
        BigInteger n2 = BigInteger.ZERO;
        assertEquals(n2, SumN.sum(n1));
    }
}
