import java.math.BigInteger;

public class FactorialAction extends Action{
    public FactorialAction() {
        this.description = MyView.factorialActionDescription;
    }

    public void doSmth(int... v) {
        BigInteger n = BigInteger.valueOf(v[0]);
        if (n.compareTo(BigInteger.valueOf(0)) > 0) {
            BigInteger f = BigInteger.valueOf(1);
            BigInteger i = BigInteger.valueOf(1);
            while (i.compareTo(n) == -1) {
                f = f.multiply(i);
                i = i.add(BigInteger.valueOf(1));
            }
            System.out.println("Факториал числа n равен: " + f.multiply(n));
        } else if (n.compareTo(BigInteger.valueOf(0)) == 0) {
            System.out.println(1);
        } else {
            System.out.println("Error");
        }
    }

    public int getNumberArguments() {
        return 1;
    }

    public String getDescription() {
        return this.description;
    }
}
