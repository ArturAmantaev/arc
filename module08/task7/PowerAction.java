import java.math.BigInteger;

public class PowerAction extends Action{
    public PowerAction() {
        this.description = MyView.powerActionDescription;
    }

    public void doSmth(int... v) {
        BigInteger num = BigInteger.valueOf(v[0]);
        if (num.compareTo(BigInteger.valueOf(0)) > 0) {
            BigInteger a = BigInteger.valueOf(2);
            BigInteger j = BigInteger.valueOf(1);
            while (j.compareTo(num) < 0) {
                a = a.multiply(BigInteger.valueOf(2));
                j = j.add(BigInteger.valueOf(1));
            }
            System.out.println("2 в степени " + num + " равно: " + a);
        } else {
            System.out.println("Error");
        }
    }

    public int getNumberArguments() {
        return 1;
    }

    public String getDescription() {
        return this.description;
    }
}
