public abstract class Action {
    public String description;

    public abstract void doSmth(int... v);

    public abstract String getDescription();

    public abstract int getNumberArguments();
}