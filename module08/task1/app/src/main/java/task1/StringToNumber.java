package task1;

import java.util.Scanner;

public class StringToNumber {

    public static int kol_num(char[] arr_str) {
        int counterNumber = 0;
        for (int i = 0; i < arr_str.length; i++) {
            if ((arr_str[i] > 47 && arr_str[i] < 58) || arr_str[i] == 43 || arr_str[i] == 45) {
                counterNumber++;
            }
        }
        return counterNumber;
    }

    public static int index = 0;

    public static int addAndSubFromCharArray(char[] arr_str) {
        int number = 0;
        boolean f = false;
        while (index < arr_str.length) {
            if (arr_str[index] > 47) {
                number = number * 10 + arr_str[index] - 48;
                index++;
            } else {
                if (arr_str[index] == 43) {
                    index++;
                    number = number + numFromCharArray(arr_str);
                } else {
                    index++;
                    number = number - numFromCharArray(arr_str);
                }
            }
        }
        return number;
    }

    public static int numFromCharArray(char[] arr_str) {
        boolean f = false;
        int number = 0;
        while (index < arr_str.length && f == false) {
            if (arr_str[index] > 47) {
                number = number * 10 + arr_str[index] - 48;
                index++;
            } else {
                f = true;

            }
        }
        return number;
    }

    public static int remakeStringToNumber(String line) {
        char[] arrayLine = line.toCharArray();
        if (kol_num(arrayLine) != arrayLine.length) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return addAndSubFromCharArray(arrayLine);
    }
}
