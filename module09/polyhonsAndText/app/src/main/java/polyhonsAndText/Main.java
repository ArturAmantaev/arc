package polyhonsAndText;

public class Main {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(0,0, 10,10, 10,0);
        Triangle triangle2 = new Triangle(3,3, -1,-1,2,0);
        Triangle triangle3 = new Triangle(-14,-15, 0,10,15,0);
        System.out.println(triangle1.getSquare());
        System.out.println(triangle1.getPerimeter());
//        triangle1.openWindow(500, 1000);
        MyText myText1 = new MyText("YES");
        MyText myText2 = new MyText("NO");
//        myText1.openWindow();
        Operations operations = new Operations();
        operations.outputArrayObject(new OutputOnDisplay[]{triangle1, myText1,triangle2, triangle3, myText2}, 2);
    }
}
