package polyhonsAndText;

public class Triangle extends Polygon {
    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        super(new double[]{x1, x2, x3}, new double[]{y1,y2,y3});
    }

    public  double getSquare() {
        return 0.5 * Math.abs((x[1] - x[0]) * (y[2] - y[0]) - (x[2] - x[0]) * (y[1] - y[0]));
    }
}