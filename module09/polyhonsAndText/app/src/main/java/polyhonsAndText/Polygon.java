package polyhonsAndText;

import javax.swing.*;
import java.awt.*;

public abstract class Polygon implements OutputOnDisplay {
    double[] x;
    double[] y;

    public Polygon(int n) {
        x = new double[n];
        y = new double[n];
    }

    public Polygon(double[] x, double[] y) {
        if (x.length != y.length) {
            return;
        }
        this.x = x;
        this.y = y;
    }

    public void setX(double[] x) {
        this.x = x;
    }

    public void setY(double[] y) {
        this.y = y;
    }

    public abstract double getSquare();

    public double getPerimeter() {
        double result = 0;
        for (int i = 0; i < x.length - 1; i++) {
            result += Math.sqrt((x[i + 1] - x[i]) * (x[i + 1] - x[i]) + (y[i + 1] - y[i]) * (y[i + 1] - y[i]));
        }
        result += Math.sqrt((x[x.length - 1] - x[0]) * (x[x.length - 1] - x[0]) + (y[y.length - 1] - y[0]) * (y[y.length - 1] - y[0]));
        return result;
    }

    public void openWindow(int width, int height) {
        JFrame frame = new JFrame("Test");
        frame.setBounds(0, 0, width, height);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel contentPane = getJPanel(width, height);
        frame.setContentPane(contentPane);
    }

    public JPanel getJPanel(int width, int height) {
        JPanel contentPane = new JPanel() {
            Graphics2D g2;

            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g2 = (Graphics2D) g;
                g2.setColor(Color.BLACK);
                int halfWidth = width / 2;
                int halfHeight = height / 2;
                g2.drawLine(0, halfHeight, width, halfHeight);
                g2.drawLine(halfWidth, 0, halfWidth, height);
                g2.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

                for (int i = 0; i < x.length - 1; i++) {
                    g2.drawLine((int) x[i] * 10 + halfWidth, (int) -y[i] * 10 + halfHeight, (int) x[i + 1] * 10 + halfWidth, (int) -y[i + 1] * 10 + halfHeight);
                }
                g2.drawLine((int) x[0] * 10 + halfWidth, (int) -y[0] * 10 + halfHeight, (int) x[x.length - 1] * 10 + halfWidth, (int) -y[y.length - 1] * 10 + halfHeight);
            }
        };
        return contentPane;
    }
}