package polyhonsAndText;

import javax.swing.*;
import java.awt.*;

public class Operations {
    public void outputArrayObject(OutputOnDisplay[] o, int n) {
        JFrame jFrame = new JFrame("a");
        jFrame.setSize(1920,1080);
        JPanel jPanel = new JPanel(new GridLayout(n,o.length / 2));
        int width = 1920 / ((o.length - o.length % n + n) / n);
        int height = 1080 / n;
        System.out.println("w:" + width);
        System.out.println("h:" + height);
        for (int i = 0; i < o.length; i++) {
            jPanel.add(o[i].getJPanel(width, height));
        }
        jFrame.add(jPanel);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }
}
