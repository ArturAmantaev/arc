package polyhonsAndText;

import javax.swing.*;
import java.awt.*;

public class MyText implements OutputOnDisplay {
    String text;

    public MyText(String text) {
        this.text = text;
    }

    public void openWindow(int width, int height) {
        JFrame frame = new JFrame("Test");
        frame.setBounds(0, 0, width, height);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel contentPane = getJPanel(width, height);
        frame.setContentPane(contentPane);
    }

    public JPanel getJPanel(int width, int height) {
        JPanel contentPane = new JPanel() {
            Graphics2D g2;

            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g2 = (Graphics2D) g;
                g2.setColor(Color.BLACK);
                Font font = new Font("", Font.BOLD, 20);
                g2.setFont(font);
                g2.drawString(text, 10, 20);
            }
        };
        return contentPane;
    }
}
