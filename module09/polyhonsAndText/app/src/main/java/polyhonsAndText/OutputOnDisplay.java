package polyhonsAndText;

import javax.swing.*;

public interface OutputOnDisplay {
    void openWindow(int width, int height);
    JPanel getJPanel(int width, int height);
}
