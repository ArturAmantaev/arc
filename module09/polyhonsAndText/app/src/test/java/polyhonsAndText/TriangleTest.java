package polyhonsAndText;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TriangleTest {
    @Test void square() {
        Triangle triangle = new Triangle(0,0,6,0,3,4);
        assertEquals(12, triangle.getSquare());
    }
}