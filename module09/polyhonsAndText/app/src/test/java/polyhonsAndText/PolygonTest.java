package polyhonsAndText;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PolygonTest {
    @Test void perimeter() {
        Triangle triangle = new Triangle(0, 0, 6, 0, 3, 4);
        assertEquals(16, triangle.getPerimeter());
    }
}