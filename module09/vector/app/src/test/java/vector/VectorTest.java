package vector;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VectorTest {
    @Test void vectorX1() {
        Vector vector1 = new Vector(0,0,1,1);
        assertEquals(0, vector1.getX1());
    }

    @Test void vectorToString() {
        Vector vector1 = new Vector(0,0,1,1);
        assertEquals("0.0 0.0\n1.0 1.0", vector1.toString());
    }

    @Test void sum() {
        Vector vector1 = new Vector(0,0,1,1);
        Vector vector2 = new Vector(0,0,2,0);
        vector1.sum(vector2);
        Vector vector3 = new Vector(0,0,3,1);
        assertEquals(vector3, vector1);
    }

    @Test void sumStatic() {
        Vector vector1 = new Vector(0,0,1,1);
        Vector vector2 = new Vector(0,0,2,0);
        Vector vector3 = Vector.sum(vector1, vector2);
        Vector vectorR = new Vector(0,0,3,1);
        assertEquals(vectorR, vector3);
    }

    @Test void substract() {
        Vector vector1 = new Vector(0,0,1,1);
        Vector vector2 = new Vector(1,1,3,1);
        vector1.subtract(vector2);
        Vector vector3 = new Vector(3,1,0,0);
        assertEquals(vector3, vector1);
    }

    @Test void subtractStatic() {
        Vector vector1 = new Vector(0,0,1,1);
        Vector vector2 = new Vector(1,1,3,1);
        Vector vector3 = Vector.subtract(vector1, vector2);
        Vector vector4 = new Vector(3,1,0,0);
        assertEquals(vector4, vector3);
    }
    @Test void multiply() {
        Vector vector1 = new Vector(0,0,1,1);
        vector1.multiply(2);
        Vector vector2 = new Vector(0,0,2,2);
        assertEquals(vector2, vector1);
    }

    @Test void multiplyStatic() {
        Vector vector1 = new Vector(0,0,1,2);
        Vector vector2 = new Vector(0,0,2,4);
        Vector vector3 = Vector.multiply(vector1, 2);
        assertEquals(vector2, vector3);
    }

    @Test void division() {
        Vector vector1 = new Vector(0,0,4,8);
        vector1.division(2);
        Vector vector2 = new Vector(0,0,2,4);
        assertEquals(vector2, vector1);
    }

    @Test void divisionStatic() {
        Vector vector1 = new Vector(0,0,4,8);
        Vector vector2 = Vector.division(vector1, 2);
        Vector vector3 = new Vector(0,0,2,4);
        assertEquals(vector3, vector2);
    }
}
