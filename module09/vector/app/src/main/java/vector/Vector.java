package vector;

import java.util.Objects;

public class Vector {
    double x1;
    double x2;
    double y1;
    double y2;

    public Vector(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Vector(Vector v) {
        this.x1 = v.getX1();
        this.x2 = v.getX2();
        this.y1 = v.getY1();
        this.y2 = v.getY2();
    }

    public double getX1() {
        return this.x1;
    }

    public double getX2() {
        return this.x2;
    }

    public double getY1() {
        return this.y1;
    }

    public double getY2() {
        return this.y2;
    }

    public String toString() {
        return this.x1+" " +this.y1 + "\n" +
                this.x2+" " +this.y2;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return Double.compare(vector.x1, x1) == 0 && Double.compare(vector.x2, x2) == 0 && Double.compare(vector.y1, y1) == 0 && Double.compare(vector.y2, y2) == 0;
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), x1, x2, y1, y2);
    }

    public static Vector sum(Vector v1, Vector v2) {
        Vector resultV = new Vector(v1);
        resultV.sum(v2);
        return resultV;
    }

    public void sum(Vector v) {
        this.x2 += v.getX2();
        this.y2 += v.getY2();
    }

    public static Vector subtract(Vector v1, Vector v2) {
        Vector resultV = new Vector(v1);
        resultV.subtract(v2);
        return resultV;
    }

    public void subtract(Vector v) {
        double buffer1 = this.x1;
        double buffer2 = this.y1;
        this.x1 = this.x2 + v.getX2() - v.getX1();
        this.y1 = this.y2 + v.getY2() - v.getY1();
        this.x2 = buffer1;
        this.y2 = buffer2;
        System.out.println(x1 + " " + y1);
    }

    public static Vector multiply(Vector v, double n) {
        Vector resultV = new Vector(v);
        resultV.multiply(n);
        return resultV;
    }

    public void multiply(double n) {
        this.x2 = x2 * n;
        this.y2 = y2 * n;
    }

    public static Vector division(Vector v, double n) {
        Vector resultV = new Vector(v);
        resultV.division(n);
        return resultV;
    }

    public void division(double n) {
        this.x2 = this.x2 / n;
        this.y2 = this.y2 / n;
    }
}
