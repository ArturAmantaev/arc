package myEmployees;

public class ChiefAccountant extends Accountant {

    public ChiefAccountant(String name, String position, int salary) {
        super(name, position, salary);
    }

    public void changeSalary(Employee employee, int salary) {
        employee.setSalary(salary);
    }
}
