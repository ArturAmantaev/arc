package myEmployees;

public abstract class Employee {
    public String name;
    public String position;
    private int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    protected void setSalary(int salary) {
        this.salary = salary;
    }

    public abstract String work();

    public String toString() {
        return "Name: " + getName() + "\n" +
                "Position: " + getPosition() + "\n" +
                "Salary: " + getSalary();
    }

    public int hashCode() {
        return name.length() * 100 + position.length() * 10 + salary;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Employee) {
            if (this.name == ((Employee) obj).getName() &&
                this.position == ((Employee) obj).getPosition() &&
                this.salary == ((Employee) obj).getSalary()) {
                return true;
            }
        }
        return false;
    }

    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
