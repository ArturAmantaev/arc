package myEmployees;

public class Worker extends Employee {

    public Worker(String name, String position, int salary) {
        super.setName(name);
        super.setPosition(position);
        super.setSalary(salary);
    }

    public String work() {
        return "Я занимаюсь сложной физической работой";
    }

    public String workPhysically() {
        return "Ээээххх какая тяжелая работа";
    }
}
