package myEmployees;

public class Accountant extends Employee {

    public Accountant(String name, String position, int salary) {
        super.setName(name);
        super.setPosition(position);
        super.setSalary(salary);
    }

    public String work() {
        return "Я умею считать. Я работаю в Excel";
    }

    public String salariesOfAllEmployees(Employee[] employees) {
        String[] salaries = new String[4];
        salaries[0] = "Worker: ";
        salaries[1] = "Engineer: ";
        salaries[2] = "Accountant: ";
        salaries[3] = "ChiefAccountant: ";
        for (int i = 0; i < employees.length; i++) {
            if (employees[i] instanceof Worker) {
                salaries[0] += employees[i].getSalary() + ", ";
            }
            if (employees[i] instanceof Engineer) {
                salaries[1] += employees[i].getSalary() + ", ";
            }
            if (employees[i] instanceof Accountant) {
                salaries[2] += employees[i].getSalary() + ", ";
            }
            if (employees[i] instanceof ChiefAccountant) {
                salaries[3] += employees[i].getSalary() + ", ";
            }
        }
        String result = "";
        for (int i = 0; i < salaries.length; i++) {
            result += salaries[i] + "\n";
        }
        return result;
    }
}
