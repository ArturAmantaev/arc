package myEmployees;

public class Engineer extends Employee {

    public Engineer(String name, String position, int salary) {
        super.setName(name);
        super.setPosition(position);
        super.setSalary(salary);
    }
    public String work() {
        return "У меня много работы";
    }
}
