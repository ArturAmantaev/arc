package myEmployees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EngineerTest {
    Employee engineer = new Engineer("Bill", "beginner", 40000);

    @Test void engineerName() {
        assertEquals("Bill", engineer.getName());
    }

    @Test void engineerWork() {
        assertEquals("У меня много работы", ((Engineer)engineer).work());
    }
}
