package myEmployees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountantTest {
    Employee accountant = new Accountant("John", "trainee", 25000);
    Employee[] employees = new Employee[3];

    @Test void accountantName() {
        assertEquals("John", accountant.getName());
    }

    @Test void accountantWork() {
        assertEquals("Я умею считать. Я работаю в Excel", ((Accountant)accountant).work());
    }

    @Test void accountantSalariesOfAllEmployees() {
        employees[0] = new Worker("B", "A", 15000);
        employees[1] = new Engineer("C", "C", 30000);
        employees[2] = new Accountant("U", "U", 25000);
        assertEquals("Worker: 15000, \n" +
                "Engineer: 30000, \n" +
                "Accountant: 25000, \n" +
                "ChiefAccountant: \n", ((Accountant)accountant).salariesOfAllEmployees(employees));
    }
}
