package myEmployees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChiefAccountantTest {
    Employee chiefAccountant = new ChiefAccountant("Rick", "boss", 100000);
    Employee[] employees = new Employee[3];

    @Test void chiefAccountant() {
        assertEquals("Rick", chiefAccountant.getName());
    }

    @Test void chiefAccountantWork() {
        assertEquals("Я умею считать. Я работаю в Excel", ((ChiefAccountant)chiefAccountant).work());
    }

    @Test void chiefAccountantSalariesOfAllEmployees() {
        employees[0] = new Worker("B", "A", 15000);
        employees[1] = new Engineer("C", "C", 30000);
        employees[2] = new Accountant("U", "U", 25000);
        assertEquals("Worker: 15000, \n" +
                "Engineer: 30000, \n" +
                "Accountant: 25000, \n" +
                "ChiefAccountant: \n", ((ChiefAccountant)chiefAccountant).salariesOfAllEmployees(employees));
    }

    @Test void chiefAccountantSetSalary() {
        Worker worker = new Worker("Bob", "beginner", 15000);
        ((ChiefAccountant)chiefAccountant).changeSalary(worker, 200000);
        assertEquals(200000, worker.getSalary());
    }
}
