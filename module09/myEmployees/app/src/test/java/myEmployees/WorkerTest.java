package myEmployees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorkerTest {
    Employee worker = new Worker("Bob", "beginner", 20000);

    @Test void workersName() {
        assertEquals("Bob", worker.getName());
    }

    @Test void workerWork() {
        assertEquals("Я занимаюсь сложной физической работой", ((Worker)worker).work());
    }

    @Test void workerWorkPhysically() {
        assertEquals("Ээээххх какая тяжелая работа", ((Worker)worker).workPhysically());
    }
}
