package matrix;

public class SumNewMatrixThread implements Runnable {

    static Matrix mat1;
    static Matrix mat2;
    static Matrix res;
    int i;
    int size;
    int n;

    public SumNewMatrixThread(Matrix mat1, Matrix mat2, int i, int size) {
        this.mat1 = mat1;
        this.mat2 = mat2;
        this.i = i;
        this.size = size;
        res = new Matrix(mat1.getVerLength(), mat1.getHorLength());
    }

    public SumNewMatrixThread(int i, int size) {
        this.i = i;
        this.size = size;
    }

    public static Matrix getMatrixResult() {
        return res;
    }

    public void run() {
        for (int j = i; j < this.i + this.size; j++) {
            for (int k = 0; k < mat1.getHorLength(); k++) {
                res.setElement(j,k, (mat1.getElement(j, k)) + mat2.getElement(j, k));
            }
        }
    }
}
