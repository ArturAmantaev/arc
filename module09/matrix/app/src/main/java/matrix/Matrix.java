package matrix;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Matrix implements Serializable {

    transient int n;
    transient int m;
    int[][] arrayMatrix;
    private static final long serialVersionUID = 1L;

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        this.arrayMatrix = new int[this.n][this.m];
    }

    public Matrix(int[][] your_matrix) {
        this.n = your_matrix.length;
        this.m = your_matrix[0].length;
        this.arrayMatrix = your_matrix;
    }

    public Matrix(String way) throws IOException {
        BufferedReader inputStream = null;
        int[][] massive;
        try {
            inputStream = new BufferedReader(new FileReader("resources/matrices/" + way));
            Scanner sc = new Scanner(inputStream);
            List<String> lines = new ArrayList<>();

            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }

            this.n = lines.size();
            this.m = wordCount(lines.get(0));

            int count = 1;
            for (int i = 1; i < n; i++) {
                if (wordCount(lines.get(i)) == m) {
                    count++;
                }
            }

            massive = new int[n][m];

            for (int i = 0; i < n; i++) {
                if (wordCount(lines.get(i)) == m) {
                    for (int j = 0; j < m; j++) {
                        String[] line = lines.get(i).split(" ");
                        massive[i][j] = Integer.parseInt(line[j]);
                    }
                } else {
                    for (int j = 0; j < wordCount(lines.get(i)); j++) {
                        String[] line = lines.get(i).split(" ");
                        massive[i][j] = Integer.parseInt(line[j]);
                    }
                    for (int j = wordCount(lines.get(i)); j < this.m; j++) {
                        massive[i][j] = 0;
                    }
                }
            }

            this.arrayMatrix = massive;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public int[][] getArray() {
        return this.arrayMatrix;
    }

    public int[] getLine(int i) {
        return this.arrayMatrix[i];
    }

    public void setLine(int i, int[] line) {
        this.arrayMatrix[i] = line;
    }

    public int getElement(int i, int j) {
        return this.arrayMatrix[i][j];
    }

    public void setElement(int i, int j, int element) {
        this.arrayMatrix[i][j] = element;
    }

    public int getVerLength() {
        return this.arrayMatrix.length;
    }

    public int getHorLength() {
        return this.arrayMatrix[0].length;
    }

    public int getHorLength(int i) {
        return this.arrayMatrix[i].length;
    }

    public String toString() {
        String str = "";
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++) {
                str += this.arrayMatrix[i][j] + "\t";
            }
            str += "\n";
        }
        return str;
    }

    public void writeToFile(String name) throws IOException {
        BufferedWriter outputStream = null;
        try {
            outputStream =
                    new BufferedWriter(new FileWriter("resources/matrices_result/" + name));
            outputStream.write(toString());
            outputStream.flush();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    public int wordCount(String str) {
        int k = 0;
        int start = 0;
        if (str.charAt(0) == ' ') {
            start = 1;
        }
        for (int i = start; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                if (str.charAt(i - 1) != ' ') {
                    k++;
                }
            }
        }
        if (str.charAt(str.length() - 1) == ' ') {
            k--;
        }
        return k + 1;
    }

    public void generateMatrix(int max) {
        int[][] matrix = new int[this.n][this.m];
        for (int j = 0; j < this.n; j++) {
            for (int k = 0; k < this.m; k++) {
                double random_num = Math.random() * max;
                matrix[j][k] = (int) random_num;
            }
        }
        this.arrayMatrix = matrix;
    }

    public static Matrix add(Matrix m1, Matrix m2) throws MatrixException {
        if (m1.getVerLength() == m2.getVerLength()) {
            Matrix result_matrix = new Matrix(m1.getVerLength(), m2.getHorLength());
            for (int i = 0; i < result_matrix.getVerLength(); i++) {
                if (m1.getHorLength(i) == m2.getHorLength(i)) {
                    for (int j = 0; j < result_matrix.getHorLength(); j++) {
                        result_matrix.setElement(i, j, m1.getElement(i, j) + m2.getElement(i, j));
                    }
                } else {
                    throw new MatrixException();
                }
            }
            return result_matrix;
        } else {
            throw new MatrixException();
        }
    }

    private static Matrix[] dividingArray(int[][] array, int k) throws Exception {
        int n = array.length;
        if (n < k) {
            throw new Exception();
        }
        int wholePartOfDivision = n / k;
        int fractionalPartOfDivision = n % k;
        int cloneFactorialPartOfDivision = fractionalPartOfDivision;
        Matrix[] resultMatrix = new Matrix[k];
        int index = 0;
        for (int i = 0; i < resultMatrix.length; i++) {
            int[][] newArray;
            if (cloneFactorialPartOfDivision > 0) {
                newArray = new int[wholePartOfDivision + 1][array[i].length];
                cloneFactorialPartOfDivision--;
            } else {
                newArray = new int[wholePartOfDivision][array[i].length];
            }
            for (int j = 0; j < newArray.length; j++) {
                newArray[j] = array[index];
                index++;
            }
            resultMatrix[i] = new Matrix(newArray);
        }

        return resultMatrix;
    }

    public static void additionByThread(Matrix m1, Matrix m2) throws Exception {
        Matrix resultMatrix = null;
        int n = 3;
        Matrix[] matrix1 = dividingArray(m1.getArray(), n);
        Matrix[] matrix2 = dividingArray(m2.getArray(), n);
        SumMatrixThread[] sumArrayThreads = new SumMatrixThread[n];
        Thread[] threads = new Thread[n];
        for (int i = 0; i < n; i++) {
            sumArrayThreads[i] = new SumMatrixThread(matrix1[i], matrix2[i]);
            threads[i] = new Thread(sumArrayThreads[i]);
            threads[i].start();
        }
        for (int i = 0; i < n; i++) {
            threads[i].join();
            Matrix qwe = sumArrayThreads[i].getMatrixResult();
            System.out.println(qwe);
        }
    }

    public static int[] dividingNumberIntoParts(int n, int k) throws Exception {
        if (n < k) {
            throw new Exception();
        }
        int wholePartOfDivision = n / k;
        int fractionalPartOfDivision = n % k;
        int cloneFactorialPartOfDivision = fractionalPartOfDivision;
        int[] a = new int[k];
        int index = 0;
        for (int i = 0; i < a.length; i++) {
            int newArray;
            if (cloneFactorialPartOfDivision > 0) {
                newArray = wholePartOfDivision + 1;
                cloneFactorialPartOfDivision--;
            } else {
                newArray = wholePartOfDivision;
            }
            a[i] = newArray;
        }
        return a;
    }

    public static Matrix newAdditionByThread(Matrix m1, Matrix m2, int n) throws Exception {
        Matrix resultMatrix = new Matrix(m1.getVerLength(), m1.getHorLength());
        SumNewMatrixThread[] sumNewArrayThreads = new SumNewMatrixThread[n];
        Thread[] threads = new Thread[n];
        int[] qwe = dividingNumberIntoParts(m1.getVerLength(), n);
        int start = qwe[0];
        sumNewArrayThreads[0] = new SumNewMatrixThread(m1, m2, 0, qwe[0]);
        threads[0] = new Thread(sumNewArrayThreads[0]);
        threads[0].start();
        for (int i = 1; i < n; i++) {
            sumNewArrayThreads[i] = new SumNewMatrixThread(start, qwe[i]);
            start += qwe[i];
            threads[i] = new Thread(sumNewArrayThreads[i]);
            threads[i].start();
        }
        int k = 0;
        for (int i = 0; i < n; i++) {
            threads[i].join();
        }
        return sumNewArrayThreads[0].getMatrixResult();
    }

    public static Matrix subtract(Matrix m1, Matrix m2) throws MatrixException {
        if (m1.getVerLength() == m2.getVerLength()) {
            Matrix result_matrix = new Matrix(m1.getVerLength(), m2.getHorLength());
            for (int i = 0; i < result_matrix.getVerLength(); i++) {
                if (m1.getHorLength(i) != m2.getHorLength(i)) {
                    for (int j = 0; j < result_matrix.getHorLength(); j++) {
                        result_matrix.setElement(i, j, m1.getElement(i, j) - m2.getElement(i, j));
                    }
                } else {
                    throw new MatrixException();
                }
            }
            return result_matrix;
        } else {
            throw new MatrixException();
        }
    }

    public static Matrix multiply(Matrix m1, Matrix m2) throws MatrixException {
        if (m1.getHorLength() == m2.getVerLength()) {
            int[][] result_array = new int[m1.getVerLength()][m2.getHorLength()];
            for (int i = 0; i < m1.getVerLength(); i++) {
                if (m1.getHorLength(i) == m2.getVerLength()) {
                    for (int j = 0; j < m2.getHorLength(); j++) {
                        for (int y = 0; y < m2.getVerLength(); y++) {
                            result_array[i][j] = result_array[i][j] + m1.getElement(i, y) * m2.getElement(y, j);
                        }
                    }
                } else {
                    throw new MatrixException();
                }
            }
            return new Matrix(result_array);
        } else {
            throw new MatrixException();
        }
    }

}