package matrix;

public class SumMatrixThread implements Runnable{

    private Matrix matrix1;
    private Matrix matrix2;
    private Matrix matrixResult;

    public Matrix getMatrixResult() {
        return matrixResult;
    }

    public SumMatrixThread(Matrix array1, Matrix array2) {
        this.matrix1 = array1;
        this.matrix2 = array2;
    }

    public void compute() {
        this.matrixResult = new Matrix(matrix1.getVerLength(), matrix2.getHorLength());
        for (int i = 0; i < this.matrixResult.getVerLength(); i++) {
            for (int j = 0; j < this.matrixResult.getHorLength(); j++) {
                this.matrixResult.setElement(i, j, matrix1.getElement(i, j) + matrix2.getElement(i, j));
            }
        }
    }

    public void run() {
        compute();
    }
}
