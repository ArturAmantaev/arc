package matrix;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class MatrixTest {

    int[][] a1 = {{1,2,3},{4,5,6},{7,8,9}};
    int[][] a2 = {{0,5,0},{1,4,2},{3,7,2}};
    Matrix m1 = new Matrix(a1);
    Matrix m2 = new Matrix(a2);

    @BeforeEach void setArray() {
        int[][] a1 = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] a2 = {{0,5,0},{1,4,2},{3,7,2}};
    }

    @Test void getArray() {
        Matrix matrix = new Matrix(a1);
        assertArrayEquals(a1, matrix.getArray());
    }

    @Test void getVerLength() {
        Matrix matrix = new Matrix(a1);
        assertEquals(3, matrix.getVerLength());
    }

    @Test void getHorLength() {
        Matrix matrix = new Matrix(a1);
        assertEquals(3, matrix.getHorLength());
    }

    @Test void matrixAdd() throws MatrixException {
        Matrix m1 = new Matrix(a1);
        Matrix m2 = new Matrix(a2);
        Matrix m3 = Matrix.add(m1,m2);
        int[][] a3 = m3.getArray();
        int[][] a4 = {{1,7,3},{5,9,8},{10,15,11}};
        assertArrayEquals(a4, a3);
    }

    @Test void matrixMultiply() throws MatrixException {
        Matrix m1 = new Matrix(a1);
        Matrix m2 = new Matrix(a2);
        Matrix m3 = Matrix.multiply(m1,m2);
        int[][] a3 = m3.getArray();
        int[][] a4 = {{11,34,10},{23,82,22},{35,130,34}};
        assertArrayEquals(a4, a3);
    }

    @Test void matrixAdditionByThread() throws Exception {
        Matrix m1 = new Matrix(a1);
        Matrix m2 = new Matrix(a2);
        Matrix m3 = Matrix.newAdditionByThread(m1,m2,3);
        int[][] a3 = m3.getArray();
        int[][] a4 = {{1,7,3},{5,9,8},{10,15,11}};
        assertArrayEquals(a4,a3);
    }
}
